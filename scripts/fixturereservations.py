import requests
import yaml
import os.path

from datetime import date, datetime, timedelta, timezone


def readyaml(s: str):
    """
    read a yaml and returns o dict
    """
    with open(os.path.join("data", "reservations", f"{s}.yaml")) as f:
        doc = yaml.safe_load(f)
    return doc


def submit_reservation(doc):
    resp = requests.post("http://bycco.decrop.net/api/a/reservations", json=doc)
    if resp.status_code != 200:
        print(f"Failed {resp.status_code} {resp.json()}")


if __name__ == "__main__":
    submit_reservation(readyaml("jimi"))
    submit_reservation(readyaml("beatles"))
    submit_reservation(readyaml("tcmatic"))
    print("done")
