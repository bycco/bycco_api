from __future__ import print_function
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from google.oauth2 import service_account

SCOPES = ["https://www.googleapis.com/auth/drive"]


def main():
    creds = service_account.Credentials.from_service_account_file(
        "secrets/byccoreservations.json", scopes=SCOPES
    )
    delegated_credentials = creds.with_subject("ruben.decrop@bycco.be")
    service = build("drive", "v3", credentials=delegated_credentials)

    # find the Website content folder
    results = (
        service.files()
        .list(
            corpora="allDrives",
            supportsAllDrives=True,
            includeItemsFromAllDrives=True,
            q="mimeType='application/vnd.google-apps.folder' and name='Website content'",
            fields="files(id, name, driveId)",
        )
        .execute()
    )
    if not (items := results.get("files", [])):
        print("No files found.")
        return
    wcId = items[0].get("id")
    driveId = items[0].get("driveId")

    results = (
        service.files()
        .list(
            corpora="drive",
            driveId=driveId,
            supportsAllDrives=True,
            includeItemsFromAllDrives=True,
            q=f"'{wcId}' in parents",
            fields="files(id, name)",
        )
        .execute()
    )
    for f in results.get("files", []):
        print(f.get("name"), f.get("id"))


if __name__ == "__main__":
    main()
