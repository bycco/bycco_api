import asyncio

from datetime import date, datetime, timedelta, timezone
from motor.motor_asyncio import AsyncIOMotorClient
from reddevil.common import get_db, register_app
from bycco.service.reservation import createReservation
from bycco.models.md_reservation import (
    ReservationIn,
    Guest,
)

from bycco.db.db_reservation import DbReservation


async def main():
    """
    prepares the database for testing the registration
     - drop the database
     - create an event
     - create 3 members
     - create 2 topics
    """
    register_app(settingsmodule="bycco.settings")
    db = get_db()
    db.drop_collection(DbReservation.COLLECTION)

    # create account
    await createReservation(
        ReservationIn(
            address="Wetstraat 16, 1000 Brussel",
            checkindate="2011-11-11",
            checkoutdate="2011-11-14",
            email="kanibaal@koersen.be",
            first_name="Eddy",
            guestlist=[
                Guest(birthdate="1970-01-01", first_name="Julius", last_name="Caeser"),
                Guest(
                    birthdate="1990-12-31", first_name="Wilfried", last_name="Martens"
                ),
            ],
            last_name="Merckx",
            locale="nl",
            lodging="",
            mobile="012345",
        )
    )


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
