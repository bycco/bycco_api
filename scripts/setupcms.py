import asyncio
from fastapi import FastAPI
from reddevil.common import get_db, register_app, get_settings  # noqa

register_app(FastAPI(), "bycco.settings", "/api")
settings = get_settings()
db = get_db()
print("db", db)

from reddevil.service.sv_page import (
    createPage,
    updatePage,
)
from reddevil.models.md_page import (
    PageIn,
    PageUpdate,
    PageI18nFields,
)

from reddevil.models.md_i18nfield import I18nField


async def main():
    """
    prepares the database for testing pages
    """
    # drop database
    print("dropping database")

    cl = db.client
    await cl.drop_database("bycco")

    # create page
    page_id = await createPage(
        PageIn(
            doctype="normal-page",
            name="home",
            locale="nl",
        )
    )

    # update page
    pu = PageUpdate(
        publicationdate="2020-08-20",
        i18n={
            "default": PageI18nFields(
                body="Home content", intro="Hpme Intro", title="Home Page"
            ),
            "nl": PageI18nFields(body="Inhoud", intro="Intro", title="Homepagina"),
        },
    )
    await updatePage(page_id, pu)


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
