import asyncio
from bycco.main import settings
from bycco.db import connect_mongodb, get_mongodb
from bycco.service.enrollment import get_natstatus


async def run(db):
    coll = db.enrollment
    async for e in coll.find({}):
        fidenat = e.get("nationalityfide", "")
        natstatus = e.get("natstatus", "")
        print(f'{e["first_name"]} {e["last_name"]}: {fidenat} {natstatus}')
        if not natstatus:
            natstatus = get_natstatus(fidenat)
            print(f"Changing natstatus to {natstatus}")
            await coll.update_one({"_id": e["_id"]}, {"$set": {"natstatus": natstatus}})


async def main():
    await connect_mongodb()
    db = await get_mongodb()
    await run(db)


if __name__ == "__main__":
    asyncio.run(main())
