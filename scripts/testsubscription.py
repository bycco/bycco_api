import asyncio

from datetime import date, datetime, timedelta, timezone
from bycco import settings
from bycco.db import get_db

from bycco.service.enrollment import (
    addEnrollment,
    confirmEnrollment,
    deleteEnrollment,
    get_enrollment,
    get_enrollments,
    updateEnrollment,
    checkId,
    csv_enrollments,
)
from bycco.models.md_enrollment import (
    EnrollmentIn,
    EnrollmentCategory,
    EnrollmentList,
    EnrollmentDetailedOut,
    EnrollmentOut,
    EnrollmentOptional,
)


async def main():

    # # drop database
    # print('dropping database')
    # db = get_db()
    # cl = db.client
    # await cl.drop_database('bycco')

    # r = await checkId('45608')
    # print('checkid', r)
    # # add a enrollment
    # id1 = await addEnrollment(EnrollmentIn(
    #     locale='nl',
    #     category=EnrollmentCategory.B8,
    #     idbel='45608',
    # ))
    # r = await checkId('45608')
    # print('checkid', r)
    # id2 = await addEnrollment(EnrollmentIn(
    #     locale='nl',
    #     category=EnrollmentCategory.G12,
    #     idbel='28436',
    # ))
    # # s = await getEnrollment(id1)
    # # ss = await getEnrollments()
    # ss = await updateEnrollment(id1, EnrollmentOptional(
    #     emailplayer='ruben@decrop.net'
    # ))
    # ss = await confirmEnrollment(id1)
    # r = await checkId('45608')
    # print('checkid', r)
    csvoutput = await csv_enrollments()
    print("csvoutpout", csvoutput)


if __name__ == "__main__":
    asyncio.run(main())
