import csv

emails = set()

with open("data/allemail.csv") as f:
    reader = csv.reader(f)
    for row in reader:
        if row[0] and "@" in row[0]:
            emails.add(row[0].strip().lower())
        if row[1] and "@" in row[1]:
            emails.add(row[1].strip().lower())
        if row[2] and "@" in row[2]:
            emails.add(row[2].strip().lower())
        if row[3] and "@" in row[3]:
            emails.add(row[3].strip().lower())

sortedemails = sorted(emails)

with open("data/setemails.txt", "w") as f:
    for em in sortedemails:
        f.write(f"{em}\n")
