# copyright Ruben Decrop 2012 - 2015
# copyright Chessdevil Consulting BVBA 2015 - 2020

import logging
from fastapi import HTTPException
from fastapi.responses import HTMLResponse
from fastapi.security.api_key import APIKeyHeader

from reddevil.core import RdException, get_settings
from bycco.main import app

from bycco.service.adhoc import (
    update_elo,
    enrollment2attendee,
    # rsv2eaters,
    print_prizes,
)


log = logging.getLogger(__name__)
settings = get_settings()
api_key_header = APIKeyHeader(name="X-API-Key", auto_error=False)


# @app.get("/api/v1/sheet")
# async def api_sheet_download(
#     req: Request,
#     api_key: APIKey = Depends(api_key_header),
# ):
#     log.info(f"headers {req.headers}")
#     if not api_key:
#         raise HTTPException(status_code=401, detail="MissingAPIKey")
#     if api_key != settings.APIKEY:
#         raise HTTPException(status_code=401, detail="WrongAPIKey")
#     try:
#         enr = await get_enrollments({"_class": EnrollmentOut})
#         rsv = await get_reservations()
#         prq = await get_payment_requests()
#         rs = enr.dict()
#         rs.update(rsv.dict())
#         rs.update(prq.dict())
#         return rs
#     except RdException as e:
#         raise HTTPException(status_code=e.status_code, detail=e.description)
#     except:
#         log.exception("failed api call get_enrollments")
#         raise HTTPException(status_code=500, detail="Internal Server Error")


# @app.get("/api/v1/sheetenr")
# async def api_sheet_rsv(
#     api_key: APIKey = Depends(api_key_header),
# ):
#     if not api_key:
#         raise HTTPException(status_code=401, detail="MissingAPIKey")
#     if api_key != settings.APIKEY:
#         raise HTTPException(status_code=401, detail="WrongAPIKey")
#     try:
#         enr = await get_enrollments({"_class": EnrollmentOut})
#         return enr.dict()
#     except RdException as e:
#         raise HTTPException(status_code=e.status_code, detail=e.description)
#     except:
#         log.exception("failed api call get_enrollments")
#         raise HTTPException(status_code=500, detail="Internal Server Error")


@app.post("/api/adhoc/update_elo")
async def api_update_elo():
    try:
        return await update_elo()
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api call update_elo")
        raise HTTPException(status_code=500, detail="Internal Server Error")


@app.post("/api/adhoc/enrollment2attendee")
async def api_enrollment2attendee():
    try:
        return await enrollment2attendee()
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api call enrollment2attendee")
        raise HTTPException(status_code=500, detail="Internal Server Error")


# @app.post("/api/adhoc/rsv2eaters")
# async def api_enrollment2attendee():
#     try:
#         return await rsv2eaters()
#     except RdException as e:
#         raise HTTPException(status_code=e.status_code, detail=e.description)
#     except:
#         log.exception("failed api call rsv2eaters")
#         raise HTTPException(status_code=500, detail="Internal Server Error")


# @app.get("/api/adhoc/badgemissing", response_class=HTMLResponse)
# async def api_get_attest():
#     try:
#         return await badgeMissing()
#     except RdException as e:
#         raise HTTPException(status_code=e.status_code, detail=e.description)
#     except:
#         log.exception("failed api call get_participants")
#         raise HTTPException(status_code=500, detail="Internal Server Error")


@app.get("/api/v1/prizes", response_class=HTMLResponse)
async def api_get_badges(cat: str = "U8"):
    try:
        return await print_prizes(cat)
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api call print_prizes")
        raise HTTPException(status_code=500, detail="Internal Server Error")


@app.post("/api/v1/prq/enr/generate", response_class=HTMLResponse)
async def api_generate_prq_enr():
    from bycco.service.paymentrequest import generate_prq_enr

    try:
        return await generate_prq_enr()
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api generate_prq_enr")
        raise HTTPException(status_code=500, detail="Internal Server Error")


@app.post("/api/v1/prq/enr/send", response_class=HTMLResponse)
async def api_generate_prq_enr():
    from bycco.service.paymentrequest import email_prq_enr

    try:
        return await email_prq_enr()
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api email prq enr")
        raise HTTPException(status_code=500, detail="Internal Server Error")
