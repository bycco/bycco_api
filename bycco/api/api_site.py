# copyright Ruben Decrop 2012 - 2015
# copyright Chessdevil Consulting BVBA 2015 - 2021

import logging
from reddevil.core import RdException
from fastapi import HTTPException
from bycco.main import app
from bycco.service.site import fetchI18n

log = logging.getLogger("bycco")


@app.post("/api/site/fetchi18n")
async def api_fetchMarkdownFiles():
    try:
        await fetchI18n()
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api call generate_site")
        raise HTTPException(status_code=500)
