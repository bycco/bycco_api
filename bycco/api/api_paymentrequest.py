# copyright Ruben Decrop 2012 - 2015
# copyright Chessdevil Consulting BVBA 2015 - 2020

import logging
from fastapi import HTTPException, Depends
from fastapi.security import HTTPAuthorizationCredentials


# from fastapi.security.api_key import APIKeyHeader, APIKey
from reddevil.core import RdException, get_settings, bearer_schema, validate_token

from bycco.main import app
from bycco.service.paymentrequest import (
    create_pr_enrollment,
    create_pr_reservation,
    delete_pr_enrollment,
    delete_pr_reservation,
    email_paymentrequest,
    get_payment_requests,
    get_payment_request,
    update_payment_request,
    update_pr_reservation,
)

from bycco.models.md_paymentrequest import (
    PaymentRequest,
    PaymentRequestList,
)

log = logging.getLogger("bycco")
settings = get_settings()
# api_key_header = APIKeyHeader(name="X-API-Key", auto_error=False)


# crud


@app.get("/api/v1/paymentrequest/{prqid}", response_model=PaymentRequest)
async def api_get_paymentrequests(
    prqid: str,
    auth: HTTPAuthorizationCredentials = Depends(bearer_schema),
):
    try:
        await validate_token(auth)
        return await get_payment_request(prqid)
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api call get_enrollments")
        raise HTTPException(status_code=500, detail="Internal Server Error")


@app.get("/api/v1/paymentrequest", response_model=PaymentRequestList)
async def api_get_paymentrequests(
    auth: HTTPAuthorizationCredentials = Depends(bearer_schema),
):
    try:
        await validate_token(auth)
        return await get_payment_requests()
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api call get_enrollments")
        raise HTTPException(status_code=500, detail="Internal Server Error")


@app.put("/api/v1/paymentrequest/{id}", response_model=str)
async def api_update_paymentrequest(
    id: str,
    prq: PaymentRequest,
    auth: HTTPAuthorizationCredentials = Depends(bearer_schema),
):
    try:
        await validate_token(auth)
        return await update_payment_request(id, prq)
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api call update payment_request")
        raise HTTPException(status_code=500)


# business methods pr reservation


@app.post("/api/v1/rsv/paymentrequest/{rsvid}", response_model=str)
async def api_create_pr_reservation(
    rsvid: str,
    auth: HTTPAuthorizationCredentials = Depends(bearer_schema),
):
    try:
        await validate_token(auth)
        return await create_pr_reservation(rsvid)
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api call create_pr_reservation")
        raise HTTPException(status_code=500)


@app.put("/api/v1/rsv/paymentrequest/{rsvid}", response_model=str)
async def api_update_pr_reservation(
    rsvid: str,
    prq: PaymentRequest,
    auth: HTTPAuthorizationCredentials = Depends(bearer_schema),
):
    try:
        await validate_token(auth)
        return await update_pr_reservation(rsvid, prq)
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api call update_pr_reservation")
        raise HTTPException(status_code=500)


@app.delete("/api/v1/rsv/paymentrequest/{rsvid}")
async def api_delete_pr_reservation(
    rsvid: str,
    auth: HTTPAuthorizationCredentials = Depends(bearer_schema),
):
    try:
        await validate_token(auth)
        await delete_pr_reservation(rsvid)
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api call delete_pr_reservation")
        raise HTTPException(status_code=500)


@app.post("/api/v1/prq/{prqid}/email")
async def api_email_paymentrequest(
    prqid: str,
    auth: HTTPAuthorizationCredentials = Depends(bearer_schema),
):
    try:
        await validate_token(auth)
        await email_paymentrequest(prqid)
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api call create_pr_reservation")
        raise HTTPException(status_code=500)


# business methods pr enrollments


@app.post("/api/v1/enr/paymentrequest/{enrid}", response_model=str)
async def api_create_pr_enrollment(
    enrid: str,
    admincost: str = "#NA",
    auth: HTTPAuthorizationCredentials = Depends(bearer_schema),
):
    try:
        await validate_token(auth)
        return await create_pr_enrollment(enrid, admincost=admincost)
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api call create_pr_enrollment")
        raise HTTPException(status_code=500)


@app.delete("/api/v1/enr/paymentrequest/{enrid}")
async def api_delete_pr_enrollment(
    enrid: str,
    auth: HTTPAuthorizationCredentials = Depends(bearer_schema),
):
    try:
        await validate_token(auth)
        await delete_pr_enrollment(enrid)
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api call delete_pr_enrollment")
        raise HTTPException(status_code=500)
