# copyright Ruben Decrop 2012 - 2015
# copyright Chessdevil Consulting BVBA 2015 - 2020

import logging
from fastapi import HTTPException
from fastapi.responses import HTMLResponse, Response
from fastapi import HTTPException, Depends
from fastapi.security import HTTPAuthorizationCredentials

from reddevil.core import RdException, bearer_schema, validate_token, get_settings
from bycco.main import app
from bycco.service.attendee import (
    add_attendee,
    get_attendee,
    get_attendees,
    delete_attendee,
    update_attendee,
    get_badges,
    get_badges_ids,
    get_photo,
    upload_photo,
)
from bycco.service.enrollment import get_namecards, get_namecards_ids
from bycco.models.md_attendee import (
    Attendee,
    AttendeeIn,
    AttendeeList,
    AttendeeUpdate,
)


log = logging.getLogger("bycco")
settings = get_settings()

# CRUD


@app.get("/api/v1/attendees", response_model=AttendeeList)
async def api_get_attendees(
    auth: HTTPAuthorizationCredentials = Depends(bearer_schema),
    cat: str = "",
    ss: str = "",
):
    try:
        await validate_token(auth)
        return await get_attendees(dict(cat=cat, ss=ss))
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api call get_attendees")
        raise HTTPException(status_code=500, detail="Internal Server Error")


@app.post("/api/v1/attendees", response_model=str)
async def api_add_attendee(
    att: AttendeeIn,
    auth: HTTPAuthorizationCredentials = Depends(bearer_schema),
):
    try:
        await validate_token(auth)
        return await add_attendee(att)
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api call get_attendees")
        raise HTTPException(status_code=500, detail="Internal Server Error")


@app.get("/api/v1/attendee/{id}", response_model=Attendee)
async def api_get_attendee(id: str):
    try:
        return await get_attendee(id)
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api call get_attendee")
        raise HTTPException(status_code=500, detail="Internal Server Error")


@app.delete("/api/v1/attendee/{id}")
async def api_delete_attendee(id: str):
    try:
        await delete_attendee(id)
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api call delete_attendee")
        raise HTTPException(status_code=500, detail="Internal Server Error")


@app.put("/api/v1/attendee/{id}")
async def api_update_attendee(
    id: str,
    s: AttendeeUpdate,
):
    try:
        await update_attendee(id, s)
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api call update_attendee")
        raise HTTPException(status_code=500, detail="Internal Server Error")


@app.get("/api/v1/attendees/badge", response_class=HTMLResponse)
async def api_get_badges(cat: str = None):
    try:
        # await validate_token(auth)
        return await get_badges(cat)
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api call get_badges")
        raise HTTPException(status_code=500, detail="Internal Server Error")


@app.get("/api/v1/attendees/badge_ids", response_class=HTMLResponse)
async def api_get_badges_ids(id: str = None):
    try:
        # await validate_token(auth)
        ids = id.split(",")
        return await get_badges_ids(ids)
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api call get_badges")
        raise HTTPException(status_code=500, detail="Internal Server Error")


@app.get("/api/v1/attendees/namecard", response_class=HTMLResponse)
async def api_get_namecards(cat: str = "U8"):
    try:
        return await get_namecards(cat)
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api call get_namecards")
        raise HTTPException(status_code=500, detail="Internal Server Error")


@app.get("/api/v1/attendees/namecard_ids", response_class=HTMLResponse)
async def api_get_namecards(id: str):
    try:
        ids = id.split(",")
        return await get_namecards_ids(ids)
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api call get_namecards")
        raise HTTPException(status_code=500, detail="Internal Server Error")


@app.get("/api/v1/attendee/{id}/photo", response_class=Response)
async def api_get_photo(id: str):
    try:
        return await get_photo(id)
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api call get_participant")
        raise HTTPException(status_code=500, detail="Internal Server Error")


@app.post("/api/v1/attendee/{id}/photo")
async def api_upload_photo(
    id: str,
    body: dict,
):
    try:
        await upload_photo(id, body["photo"])
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api call update_participant")
        raise HTTPException(status_code=500, detail="Internal Server Error")
