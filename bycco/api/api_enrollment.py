# copyright Ruben Decrop 2012 - 2015
# copyright Chessdevil Consulting BVBA 2015 - 2020

import logging
import base64
from fastapi import HTTPException, BackgroundTasks, Security, Depends
from fastapi.security import HTTPAuthorizationCredentials
from fastapi.security.api_key import APIKeyHeader, APIKey
from fastapi.responses import HTMLResponse, Response
from pydantic import BaseModel
from typing import List

from starlette.background import BackgroundTasks
from reddevil.core import RdException, bearer_schema, validate_token, get_settings
from bycco.main import app
from bycco.service.enrollment import (
    check_id,
    confirm_enrollment,
    create_enrollment,
    disable_enrollment,
    get_enrollment,
    get_enrollments,
    get_photo,
    update_enrollment,
    upload_photo,
    xls_enrollments,
)
from bycco.models.md_enrollment import (
    Enrollment,
    EnrollmentIn,
    EnrollmentOut,
    EnrollmentList,
    EnrollmentPublic,
    EnrollmentUpdate,
    CheckIdReply,
)


log = logging.getLogger(__name__)
settings = get_settings()

# mgmt


@app.get("/api/v1/enrollment", response_model=EnrollmentList)
async def api_get_enrollments(
    auth: HTTPAuthorizationCredentials = Depends(bearer_schema),
):
    try:
        await validate_token(auth)
        return await get_enrollments({"_class": EnrollmentOut})
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api call get_enrollments")
        raise HTTPException(status_code=500, detail="Internal Server Error")


@app.get("/api/v1/enrollment/{id}", response_model=Enrollment)
async def api_get_enrollment(
    id: str, auth: HTTPAuthorizationCredentials = Depends(bearer_schema)
):
    log.info(f"get enrollment {id} {auth}")
    try:
        await validate_token(auth)
        a = await get_enrollment(id, {"_class": EnrollmentOut})
        log.info(f"a: {a}")
        return a
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api call get_enrollment")
        raise HTTPException(status_code=500, detail="Internal Server Error")


@app.delete("/api/v1/enrollment/{id}")
async def api_delete_enrollment(
    id: str, auth: HTTPAuthorizationCredentials = Depends(bearer_schema)
):
    await validate_token(auth)
    try:
        await disable_enrollment(id)
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api call delete_enrollment")
        raise HTTPException(status_code=500, detail="Internal Server Error")


@app.put("/api/v1/enrollment/{id}")
async def api_update_enrollment(
    id: str,
    s: EnrollmentUpdate,
    auth: HTTPAuthorizationCredentials = Depends(bearer_schema),
):
    try:
        await validate_token(auth)
        await update_enrollment(id, s)
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api call update_enrollment")
        raise HTTPException(status_code=500, detail="Internal Server Error")


@app.get("/api/v1/xls/enrollment")
async def api_xls_enrollments(
    auth: HTTPAuthorizationCredentials = Depends(bearer_schema),
):
    await validate_token(auth)
    try:
        xlsfile = await xls_enrollments()
        return {"xls64": base64.b64encode(xlsfile)}
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api call get_enrollments")
        raise HTTPException(status_code=500, detail="Internal Server Error")


# anon


@app.get("/api/a/enrollments", response_model=EnrollmentList)
async def api_anon_get_enrollments():
    try:
        return await get_enrollments(
            {"_class": EnrollmentPublic, "confirmed": {"$exists": True}}
        )
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api call get_enrollments")
        raise HTTPException(status_code=500, detail="Internal Server Error")


@app.post("/api/a/enrollments/add", response_model=str)
async def api_anon_create_enrollment(s: EnrollmentIn):
    try:
        id = await create_enrollment(s)
        log.info(f"api add {id}")
        return id
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api call create_enrollment")
        raise HTTPException(status_code=500, detail="Internal Server Error")


@app.post("/api/a/enrollment/{id}/photo")
async def api_anon_upload_photo(id: str, body: dict):
    try:
        return await upload_photo(id, body["photo"])
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api call upload_photo")
        raise HTTPException(status_code=500, detail="Internal Server Error")


@app.post("/api/a/enrollment/{id}/confirm")
async def api_anon_confirm_enrollment(id: str, bt: BackgroundTasks):
    try:
        await confirm_enrollment(id, bt)
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api call get_enrollment")
        raise HTTPException(status_code=500, detail="Internal Server Error")


@app.get("/api/a/enrollment/{idbel}/check", response_model=CheckIdReply)
async def api_anon_check_id(idbel: str):
    try:
        log.info(f"anon_check_id {idbel}")
        return await check_id(idbel)
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api call get_enrollment")
        raise HTTPException(status_code=500, detail="Internal Server Error")


# business methods


@app.get("/api/a/photo/{idbel}")
async def api_anon_get_photo(idbel: str):
    try:
        return await get_photo(idbel)
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api call get_photo")
        raise HTTPException(status_code=500, detail="Internal Server Error")
