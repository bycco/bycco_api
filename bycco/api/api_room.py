# copyright Ruben Decrop 2012 - 2015
# copyright Chessdevil Consulting BVBA 2015 - 2020

import logging
from fastapi import HTTPException, Depends
from fastapi.security import HTTPAuthorizationCredentials
from reddevil.core import RdException, bearer_schema
from reddevil.core import validate_token
from bycco.main import app
from bycco.service.room import (
    get_rooms,
    get_room,
    get_free_rooms,
    update_room,
)
from bycco.models.md_room import (
    Room,
    RoomList,
)

log = logging.getLogger("bycco")


@app.get("/api/v1/room", response_model=RoomList)
async def api_get_rooms(
    auth: HTTPAuthorizationCredentials = Depends(bearer_schema),
):
    try:
        await validate_token(auth)
        return await get_rooms()
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api call get_rooms")
        raise HTTPException(status_code=500)


@app.get("/api/v1/room/{id}", response_model=Room)
async def api_get_room(
    id: str,
    auth: HTTPAuthorizationCredentials = Depends(bearer_schema),
):
    try:
        await validate_token(auth)
        return await get_room(id)
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api call get_rooms")
        raise HTTPException(status_code=500)


@app.put("/api/v1/room", response_model=Room)
async def api_update_room(auth: HTTPAuthorizationCredentials = Depends(bearer_schema)):
    try:
        await validate_token(auth)
        return await update_room()
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api call create_room")
        raise HTTPException(status_code=500)


@app.get("/api/v1/freeroom/{roomtype}", response_model=RoomList)
async def api_get_free_room(roomtype: str):
    try:
        return await get_free_rooms(roomtype)
    except RdException as e:
        raise HTTPException(status_code=e.status_code, detail=e.description)
    except:
        log.exception("failed api call create_room")
        raise HTTPException(status_code=500)
