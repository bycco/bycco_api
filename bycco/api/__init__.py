# copyright Ruben Decrop 2012 - 2015
# copyright Chessdevil Consulting BVBA 2015 - 2019

from bycco.main import app, settings


@app.get("/api")
def api_root():
    return {"hello": "world"}


# if settings.ADHOC:
import bycco.api.api_adhoc

import bycco.api.api_reservation
import bycco.api.api_room

import bycco.api.api_enrollment
import bycco.api.api_paymentrequest
import bycco.api.api_site
import bycco.api.api_test
import bycco.api.api_attendee

# import reddevil.api.api_page
import reddevil.account

# import reddevil.file.api_file
