# copyright Ruben Decrop 2012 - 2015
# copyright Chessdevil Consulting BVBA 2015 - 2021

# this file contains various test api points to call
# service points during developent


import logging
from reddevil.core import get_mongodb
from bycco.main import app
from bycco.service.mail import test_mail

log = logging.getLogger("bycco")


@app.post("/api/test/mail", response_model=str)
def api_test_mail():
    test_mail()


@app.post("/api/test/mongo", response_model=str)
async def api_test_mongo():
    try:
        db = await get_mongodb()
        db.test.insert_one({"a": 12})
    except:
        log.exception("Cannot insert testrecord")
        return "Failed"
    return "OK"


from bycco.db.mysql_signaletique import DbSignaletique


@app.get("/api/test/mysql")
async def api_test_mysql():
    rs = await DbSignaletique.getMember("45608")
    log.info(f"rs mysqlq {rs}")
