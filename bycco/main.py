# copyright Ruben Decrop 2012 - 2021
# parts are copyrighted Chessdevil Consulting

import os.path
import logging, logging.config

from fastapi import FastAPI
from fastapi.routing import APIRoute
from reddevil.core import register_app, get_settings, connect_mongodb, close_mongodb
from bycco import version

# load and register app
app = FastAPI(
    title="Bycco",
    description="Website Belgisch jeugdkampieonschap",
    version=version,
)
register_app(app, "bycco.settings", "/api")

# get settings
settings = get_settings()
logging.config.dictConfig(settings.LOG_CONFIG)
backenddir = os.path.dirname(os.path.dirname(__file__))
rootdir = os.path.dirname(backenddir)
logger = logging.getLogger("bycco")
logger.info(f"Starting website Bycco v{version} ...")

from bycco.settings import ls

logger.info(ls)


# set up the database async handlers
app.add_event_handler("startup", connect_mongodb)
app.add_event_handler("shutdown", close_mongodb)

# import service layer

logger.info(f"Service layer loaded")

# import api endpoints
import bycco.api

logger.info(f"Api layer loaded")

#    Simplify operation IDs so that generated API clients have simpler function
#    names.
for route in app.routes:
    if isinstance(route, APIRoute):
        route.operation_id = route.name[4:]
