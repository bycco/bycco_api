# copyright Ruben Decrop 2012 - 2015
# copyright Chessdevil Consulting BVBA 2015 - 2019

# we don't use pydantic here because there is no API ysued for these models

from dataclasses import dataclass, field
from typing import List, Optional
from io import BytesIO


@dataclass
class MailAttachment:
    filename: str
    mimetype: str
    buffer: Optional[BytesIO] = None
    template: Optional[str] = None


@dataclass
class MailParams:
    subject: str
    sender: str
    receiver: str
    template: str
    locale: str
    attachments: List[MailAttachment] = field(default_factory=list)
    bcc: str = ""
    cc: str = ""
