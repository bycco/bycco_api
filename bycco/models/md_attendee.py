# copyright Ruben Decrop 2012 - 2015
# copyright Chessdevil Consulting BVBA 2015 - 2019

from datetime import datetime
from typing import Dict, Any, List, Optional, Type, Union
from enum import Enum
from pydantic import BaseModel


class AttendeeCategory(str, Enum):
    B8 = "B8"
    B10 = "B10"
    B12 = "B12"
    B14 = "B14"
    B16 = "B16"
    B18 = "B18"
    B20 = "B20"
    G8 = "G8"
    G10 = "G10"
    G12 = "G12"
    G14 = "G14"
    G16 = "G16"
    G18 = "G18"
    G20 = "G20"
    ARB = "ARB"
    ORG = "ORG"
    EAT = "EAT"
    GST = "GST"


class Gender(str, Enum):
    M = "M"
    F = "F"


class NatStatus(int, Enum):
    fidebelg = 0
    nobelg = 1
    unknown = 2


class Attendee_(BaseModel):
    """
    the attendee model as used in the database
    is normally not exposed
    """

    badgemimetype: str
    badgeimage: bytes
    badgelength: int
    birthyear: int
    category: str
    chesstitle: str
    first_name: str
    gender: Gender
    idbel: str
    idclub: str
    idfide: str
    locale: str
    last_name: str
    meals: str
    nationalityfide: str
    present: Optional[datetime]
    rating: int
    remarks: str
    _id: str
    _version: int
    _documenttype: str
    _creationtime: datetime
    _modificationtime: datetime


class Attendee(BaseModel):
    """
    the generic model used to encode results from the DB
    Normally more fine grained models are used
    """

    badgemimetype: Optional[str]
    badgeimage: Optional[bytes]
    badgelength: Optional[int]
    birthyear: Optional[int]
    category: Optional[str]
    chesstitle: Optional[str]
    custom: Optional[str]
    first_name: Optional[str]
    gender: Optional[Gender]
    id: Optional[str]
    idbel: Optional[str]
    idclub: Optional[str]
    idfide: Optional[str]
    locale: Optional[str]
    last_name: Optional[str]
    meals: Optional[str]
    rating: Optional[int]
    remarks: Optional[str]


class AttendeeList(BaseModel):
    """
    a list view of attendees
    """

    attendees: List[Any]


class AttendeeUpdate(BaseModel):
    """
    validator for attendee updates
    """

    badgemimetype: Optional[str]
    badgeimage: Optional[bytes]
    badgelength: Optional[int]
    birthyear: Optional[int]
    category: Optional[str]
    chesstitle: Optional[str]
    custom: Optional[str]
    first_name: Optional[str]
    gender: Optional[Gender]
    id: Optional[str]
    idbel: Optional[str]
    idclub: Optional[str]
    idfide: Optional[str]
    locale: Optional[str]
    last_name: Optional[str]
    meals: Optional[str]
    rating: Optional[int]
    remarks: Optional[str]


class AttendeeIn(BaseModel):
    """
    validator for adding an Attendee
    """

    category: str
    first_name: str
    last_name: str
    meals: Optional[str] = "NO"
    remarks: Optional[str] = ""
