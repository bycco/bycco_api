# copyright Ruben Decrop 2012 - 2015
# copyright Chessdevil Consulting BVBA 2015 - 2019

from datetime import datetime
from typing import Dict, Any, List, Optional, Type, Union
from enum import Enum
from pydantic import BaseModel, Field


class Guest(BaseModel):
    age_category: Optional[str]
    birthday: Optional[str]
    first_name: Optional[str]
    player: Optional[bool]
    last_name: Optional[str]
    meals: Optional[List[str]]
    meals_wishes: Optional[str]
    lodging: Optional[str]


class Assignment(BaseModel):
    roomnr: Optional[str]
    roomtype: Optional[str]
    guestlist: Optional[List[Guest]]
    assignmentdate: datetime


class Reservation_(BaseModel):
    """
    the reservation model as used in the database
    is normally not exposed
    """

    address: str
    assignments: List[Assignment]
    bycco_remarks: str
    checkindate: str  # format YYYY-MM-DD
    checkoutdate: str  # format YYYY-MM-DD
    email: str  # comma separated list of email addresses
    enabled: bool
    first_name: str
    guestlist: List[Guest]
    locale: str
    lodging: str
    logging: List[str]
    last_name: str
    meals: str  # comma separated list of MMDD-BLD
    mobile: str  # comma separeted list of free format mobile numbers
    number: int  # a sequence number
    organizers: bool
    payment_id: str
    remarks: str
    _id: str
    _version: int
    _documenttype: str
    _creationtime: datetime
    _modificationtime: datetime


class Reservation(BaseModel):
    """
    Reservation model, all fields optional
    """

    address: Optional[str]
    assignments: Optional[List[Assignment]]
    bycco_remarks: Optional[str]
    checkindate: Optional[str]
    checkoutdate: Optional[str]
    email: Optional[str]
    enabled: Optional[bool]
    first_name: Optional[str]
    guestlist: Optional[List[Guest]]
    id: Optional[str]
    last_name: Optional[str]
    locale: Optional[str]
    lodging: Optional[str]
    logging: Optional[List[str]]
    meals: Optional[str]
    mobile: Optional[str]
    number: Optional[int]
    organizers: Optional[bool]
    payment_id: Optional[str]
    remarks: Optional[str]
    _version: Optional[int]
    _documenttype: Optional[str]
    creationtime: Optional[datetime] = Field(alias="_creationtime")
    modificationtime: Optional[datetime] = Field(alias="_modificationtime")


class ReservationIn(BaseModel):
    address: Optional[str] = ""
    checkindate: Optional[str]
    checkoutdate: Optional[str]
    email: str
    first_name: str
    guestlist: List[Guest]
    last_name: str
    locale: Optional[str]
    lodging: Optional[str]
    meals: Optional[str]
    mobile: str
    organizers: Optional[bool]
    remarks: Optional[str] = ""


class ReservationList(BaseModel):
    reservations: List[Any]
