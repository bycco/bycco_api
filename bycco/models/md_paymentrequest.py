# copyright Ruben Decrop 2012 - 2015
# copyright Chessdevil Consulting BVBA 2015 - 2019

from datetime import datetime
from typing import Dict, Any, List, Optional, Type, Union
from enum import Enum
from pydantic import BaseModel, Field


class PaymentDetail(BaseModel):
    description: Optional[str]
    # quantity: Optional[int]
    quantity: Optional[Any]
    unitprice: Optional[str]  # a Decimal
    totalprice: Optional[str]  # a Decimal


class PaymentRequest_:
    """
    as written in the database, for documentation purpose only
    """

    address: str
    checkindate: str  # format YYYY-MM-DD
    checkoutdate: str  # format YYYY-MM-DD
    details: List[PaymentDetail]
    email: str
    first_name: str
    guests: str
    idbel: str
    last_name: str
    link_id: str
    locale: str
    mobile: str
    number: int
    paydate: str
    paymessage: str
    paystatus: bool
    reductionamount: str
    reductionpct: str
    remarks: str
    reason: str
    totalprice: str
    totalwithdiscount: str
    sentdate: str
    _creationtime: datetime
    _documenttype: str
    _id: str
    _modificationtime: datetime
    _version: int


class PaymentRequest(BaseModel):
    """
    base for all paymentRequest
    """

    address: Optional[str]
    checkindate: Optional[str]  # format YYYY-MM-DD
    checkoutdate: Optional[str]  # format YYYY-MM-DD
    details: Optional[List[PaymentDetail]]
    email: Optional[str]  # csl of email addresses
    first_name: Optional[str]
    guests: Optional[str]  # csl of firstname lastname
    id: Optional[str]
    idbel: Optional[str]
    last_name: Optional[str]
    link_id: Optional[str]
    locale: Optional[str]
    mobile: Optional[str]
    number: Optional[int]
    reductionamount: Optional[str]
    reductionpct: Optional[str]
    remarks: Optional[str]
    paydate: Optional[str]  # format YYYY-MM-DD
    paymessage: Optional[str]  # '+++NNN-NNNN-NNNNN+++'
    paystatus: Optional[bool]
    reason: Optional[str]  # 'reservation' or 'enrollment'
    totalprice: Optional[str]  # a Decimal
    totalwithdiscount: Optional[str]  # a Decimal
    sentdate: Optional[str]  # format YYYY-MM-DD
    _version: Optional[int]
    _documenttype: Optional[str]
    creationtime: Optional[datetime]
    modificationtime: Optional[datetime]


class PaymentRequestOut(BaseModel):
    """
    validator for lisy of paymentrequest( reservation and enrollment)
    """

    email: str
    first_name: str
    id: str
    idbel: Optional[str] = ""
    locale: str
    last_name: str
    reductionamount: Optional[str]
    reductionpct: Optional[str]
    remarks: Optional[str] = ""
    room: Optional[str] = ""
    sentdate: Optional[str]
    paydate: Optional[str]
    paymessage: Optional[str]
    paynumber: Optional[int]
    price: dict
    enrollment_id: Optional[str]
    enrollment_date: Optional[str]
    creationtime: Optional[datetime]
    modificationtime: Optional[datetime]


class PaymentRequestUpdate(BaseModel):
    """
    validator for updating a PaymentRequest
    """

    address: Optional[str]
    assignmentdate: Optional[str]
    checkindate: Optional[str]  # format YYYY-MM-DD
    checkoutdate: Optional[str]  # format YYYY-MM-DD
    email: Optional[str]  # comma separated list of email addresses
    first_name: Optional[str]
    locale: Optional[str]
    lodging_assigned: Optional[str]
    lodging_requested: Optional[str]
    last_name: Optional[str]
    mobile: Optional[str]
    reductionamount: Optional[str]
    reductionpct: Optional[str]
    remarks: Optional[str]
    reservation_id: Optional[str]
    room: Optional[str]
    sentdate: Optional[str]  # format YYY-MM-DD
    paydate: Optional[str]  # format YYY-MM-DD
    paymessage: Optional[str]  # structured message in '+++NNN-NNNN-NNNNN+++'
    paynumber: Optional[int]  #
    paystatus: Optional[bool]
    price: Optional[dict]
    enrollment_date: Optional[str]
    enrollment_id: Optional[str]


class PaymentRequestList(BaseModel):
    paymentrequests: List[Any]
