# copyright Ruben Decrop 2012 - 2015
# copyright Chessdevil Consulting BVBA 2015 - 2019

from datetime import datetime
from typing import Dict, Any, List, Optional, Type
from enum import Enum
from pydantic import BaseModel, validator


class Belplayer(BaseModel):
    """
    A player in the Belgian playerlist
    """

    arbiter: Optional[str]
    birthyear: int
    club_id: int
    fide_id: Optional[int]
    fide_nationality: str
    first_name: str
    gender: str
    id: int
    last_name: str
    nationality: str
    search: str
    year: int

    @validator("arbiter")
    def arbiter_none(cls, v):
        return "" if v is None else v

    @validator("fide_id")
    def fide_none(cls, v):
        return "" if v is None else v
