# copyright Ruben Decrop 2012 - 2015
# copyright Chessdevil Consulting BVBA 2015 - 2019

from datetime import datetime
from typing import Dict, Any, List, Optional, Type, Union
from enum import Enum
from xmlrpc.client import boolean
from pydantic import BaseModel


class Room_(BaseModel):
    """
    the enrollment model as used in the database
    is normally not exposed
    """

    blocked: boolean  # blocked by organizer
    capacity: int
    enabled: boolean
    number: str
    reservation_id: str
    reservation_nr: int
    roomtype: str
    _id: str
    _version: int
    _documenttype: str
    _creationtime: datetime
    _modificationtime: datetime


class Room(BaseModel):
    """
    the generic model used to encode results from the DB
    Normally more fine grained models are used
    """

    blocked: Optional[boolean]
    capacity: Optional[int]
    enabled: Optional[boolean]
    id: Optional[str]
    number: Optional[str]
    reservation_id: Optional[str]
    reservation_nr: Optional[int]
    roomtype: Optional[str]
    _id: Optional[str]
    _version: Optional[int]
    _documenttype: Optional[str]
    _creationtime: Optional[datetime]
    _modificationtime: Optional[datetime]


class RoomOutForList(BaseModel):
    id: Optional[str]
    number: Optional[str]
    roomtype: Optional[str]
    reservation_id: Optional[str]
    reservation_nr: Optional[int]


class RoomList(BaseModel):
    """
    a list view of enrollments
    """

    rooms: List[Any]
