# copyright Ruben Decrop 2012 - 2015
# copyright Chessdevil Consulting BVBA 2015 - 2019

from datetime import datetime
from typing import Dict, Any, List, Optional, Type, Union
from enum import Enum
from pydantic import BaseModel


class EnrollmentCategory(str, Enum):
    B8 = "B8"
    B10 = "B10"
    B12 = "B12"
    B14 = "B14"
    B16 = "B16"
    B18 = "B18"
    B20 = "B20"
    G8 = "G8"
    G10 = "G10"
    G12 = "G12"
    G14 = "G14"
    G16 = "G16"
    G18 = "G18"
    G20 = "G20"
    ARB = "ARB"
    ORG = "ORG"


class Gender(str, Enum):
    M = "M"
    F = "F"


class NatStatus(str, Enum):
    fidebelg = "Fide Belg."
    nobelg = "No Belg."
    unknown = "Unknown"


class EnrollmentEvent(BaseModel):
    """
    event details
    """

    enddate: str
    eventtype: str
    startdate: str
    title: str
    options: dict


class Enrollment_(BaseModel):
    """
    the enrollment model as used in the database
    is normally not exposed
    """

    badgemimetype: str
    badgeimage: bytes
    badgelength: int
    birthyear: int
    category: EnrollmentCategory
    chesstitle: str
    confirmed: bool
    custom: Optional[str]
    emailattendant: Optional[str]
    emailparent: Optional[str]
    emailplayer: Optional[str]
    enabled: bool
    federation: str
    first_name: str
    fullnameattendant: str
    fullnameparent: str
    gender: Gender
    idbel: str
    idclub: str
    idfide: str
    locale: str
    last_name: str
    mobileattendant: str
    mobileparent: str
    mobileplayer: str
    nationalitybel: str
    nationalityfide: str
    matstatus: str
    present: Optional[datetime]
    rating: int
    ratingbel: int
    ratingfide: int
    registrationtime: datetime
    remarks: str
    enrollmentevent: EnrollmentEvent
    _id: str
    _version: int
    _documenttype: str
    _creationtime: datetime
    _modificationtime: datetime


class EnrollmentIn(BaseModel):
    """
    the model to create a enrollment
    """

    category: str
    emailparent: str
    emailplayer: str
    emailattendant: str
    fullnameattendant: str
    fullnameparent: str
    idbel: str
    locale: str
    mobileattendant: str
    mobileparent: str
    mobileplayer: str


class Enrollment(BaseModel):
    """
    the generic model used to encode results from the DB
    """

    badgemimetype: Optional[str]
    badgeimage: Optional[bytes]
    badgelength: Optional[int]
    birthday: Optional[str]
    birthyear: Optional[int]
    category: Optional[EnrollmentCategory]
    confirmed: Optional[bool]
    emailattendant: Optional[str]
    emailparent: Optional[str]
    emailplayer: Optional[str]
    enabled: Optional[bool]
    federation: Optional[str]
    first_name: Optional[str]
    fullnameattendant: Optional[str]
    fullnameparent: Optional[str]
    gender: Optional[str]
    id: Optional[str]
    idbel: Optional[str]
    idclub: Optional[str]
    idfide: Optional[str]
    locale: Optional[str]
    last_name: Optional[str]
    mobileattendant: Optional[str]
    mobileparent: Optional[str]
    mobileplayer: Optional[str]
    nationalitybel: Optional[str]
    nationalityfide: Optional[str]
    natstatus: Optional[str]
    payamount: Optional[int]
    payment_id: Optional[str]
    present: Optional[datetime]
    rating: Optional[int]
    ratingbel: Optional[int]
    ratingfide: Optional[int]
    registrationtime: Optional[datetime]
    remarks: Optional[str]
    enrollmentevent: Optional[EnrollmentEvent]


class EnrollmentList(BaseModel):
    """
    a list view of enrollments
    """

    enrollments: List[Any]


class EnrollmentOut(BaseModel):
    """
    validator for ouput
    """

    badgelength: int = 0
    birthday: Optional[str]
    birthyear: int
    category: EnrollmentCategory
    chesstitle: Optional[str]
    confirmed: bool = False
    custom: Optional[str]
    emailattendant: Optional[str]
    emailparent: Optional[str]
    emailplayer: Optional[str]
    enabled: Optional[bool]
    federation: Optional[str]
    first_name: str
    fullnameattendant: str
    fullnameparent: str
    gender: Gender
    id: str
    idbel: str
    idclub: str
    idfide: Optional[str]
    locale: str
    last_name: str
    mobileattendant: str
    mobileparent: str
    mobileplayer: str
    nationalitybel: str
    nationalityfide: str
    natstatus: Optional[str]
    payment_id: Optional[str] = ""
    present: Optional[datetime]
    rating: int = 0
    ratingbel: int = 0
    ratingfide: int = 0
    registrationtime: Optional[datetime] = None
    remarks: str = ""


class EnrollmentPublic(BaseModel):
    """
    validator for public view of a enrollment
    """

    badgelength: Optional[int] = 0
    birthday: Optional[str]
    birthyear: int
    category: EnrollmentCategory
    chesstitle: Optional[str]
    first_name: str
    gender: Gender
    id: str
    idbel: str
    idclub: str
    idfide: Optional[str]
    last_name: str
    nationalityfide: str
    rating: int = 0
    ratingbel: int = 0
    ratingfide: int = 0


class EnrollmentUpdate(BaseModel):
    """
    the generic model for updates
    """

    badgemimetype: Optional[str] = None
    badgeimage: Optional[bytes] = None
    badgelength: Optional[int] = None
    birthday: Optional[str] = None
    birthyear: Optional[int] = None
    category: Optional[EnrollmentCategory] = None
    chesstitle: Optional[str] = None
    confirmed: Optional[bool] = None
    custom: Optional[str] = None
    emailattendant: Optional[str] = None
    emailparent: Optional[str] = None
    emailplayer: Optional[str] = None
    enabled: Optional[bool] = None
    federation: Optional[str] = None
    first_name: Optional[str] = None
    fullnameattendant: Optional[str] = None
    fullnameparent: Optional[str] = None
    gender: Optional[str] = None
    idbel: Optional[str] = None
    idclub: Optional[str] = None
    idfide: Optional[str] = None
    last_name: Optional[str] = None
    locale: Optional[str] = None
    mobileattendant: Optional[str] = None
    mobileparent: Optional[str] = None
    mobileplayer: Optional[str] = None
    nationalitybel: Optional[str] = None
    nationalityfide: Optional[str] = None
    natstatus: Optional[str] = NatStatus.unknown.value
    payment_id: Optional[str] = None
    present: Optional[datetime] = None
    rating: Optional[int] = None
    ratingbel: Optional[int] = None
    ratingfide: Optional[int] = None
    registrationtime: Optional[datetime] = None
    remarks: Optional[str] = None
    enrollmentevent: Optional[EnrollmentEvent] = None


class CheckIdReply(BaseModel):
    age_ok: bool = True
    belfound: bool
    birthyear: int = 0
    first_name: Optional[str] = ""
    gender: Optional[Gender] = None
    idbel: Optional[str] = None
    idclub: Optional[str] = None
    idfide: Optional[str] = None
    last_name: Optional[str] = ""
    nationalitybel: str = "BEL"
    nationalityfide: str = ""
    natstatus: NatStatus = NatStatus.unknown
    ratingbel: int = 0
    ratingfide: int = 0
    subconfirmed: bool = False
    subid: Optional[str] = None
