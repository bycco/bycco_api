import asyncio

from fastapi import FastAPI
from reddevil.core import register_app, connect_mongodb, get_mongodb, get_settings

app = FastAPI(
    title="Bycco",
    description="Website Bycco",
    version="0",
)
register_app(app=app, settingsmodule="bycco.settings")
settings = get_settings()

from bycco.service.enrollment import get_enrollments
from bycco.models.md_enrollment import EnrollmentOut
from bycco.service.paymentrequest import create_pr_enrollment


async def main():
    """
    check if the fide id is filled in and if not   fetch it from API KBSB
    """

    # fetch all enrollments
    await connect_mongodb()
    enrollments = (await get_enrollments({
        "_class": EnrollmentOut, 
        "enabled": True,
        "confirmed": True,
    })).enrollments
    for i,enr in enumerate(enrollments):
        if enr.payment_id : 
            print(i+1, "pr existing for ", enr.first_name, enr.last_name)
        else:
            print(i+1, "creating pr for ", enr.first_name, enr.last_name)
            await create_pr_enrollment(enr.id, "0")

if __name__ == "__main__":
    asyncio.run(main())
