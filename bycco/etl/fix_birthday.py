import asyncio

from reddevil.core import register_app
from bycco.service.enrollment import get_enrollments, updateEnrollment
from bycco.service.playerlist import getBelplayer
from bycco.models.md_enrollment import EnrollmentUpdate, EnrollmentOut
from bycco.db.mysql_signaletique import DbSignaletique


async def main():
    """
    check if the fide id is filled in and if not   fetch it from API KBSB
    """
    register_app(settingsmodule="bycco.settings")

    # fetch all enrollments

    enrollments = (await get_enrollments({"_class": EnrollmentOut})).enrollments
    nrec = 0
    for s in enrollments:
        if s.birthday is None:
            await updateRecord(s.idbel.strip(), s.id)
            nrec += 1
    print(f"{nrec} updated")


async def updateRecord(idbel: str, id: str) -> bool:
    """ """
    mb = await DbSignaletique.getMember(idbel)
    assert mb is not None
    su = EnrollmentUpdate(birthday=mb[6].isoformat())
    await updateEnrollment(id, su)
    return True


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
