import asyncio

from reddevil.core import register_app
from bycco.service.enrollment import get_enrollments, updateEnrollment
from bycco.service.playerlist import getBelplayer
from bycco.models.md_enrollment import EnrollmentUpdate, EnrollmentOut


async def main():
    """
    check if the fide id is filled in and if not   fetch it from API KBSB
    """
    register_app(settingsmodule="bycco.settings")

    # fetch all enrollments

    enrollments = (await get_enrollments({"_class": EnrollmentOut})).enrollments
    nrec = 0
    for s in enrollments:
        if s.idfide is None:
            if await updateRecord(s.idbel, s.id):
                nrec += 1
    print(f"{nrec} updated")


async def updateRecord(idbel: str, id: str) -> bool:
    """ """
    bp = await getBelplayer(idbel)
    assert bp is not None
    if bp.fide_id is not None:
        print(f"{idbel} {id} need fixing {bp.fide_id}")
        su = EnrollmentUpdate(idfide=bp.fide_id)
        await updateEnrollment(id, su)
        return True
    return False


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
