import asyncio

from fastapi import FastAPI
from reddevil.core import register_app, connect_mongodb, get_mongodb, get_settings

app = FastAPI(
    title="Bycco",
    description="Website Bycco",
    version="0",
)
register_app(app=app, settingsmodule="bycco.settings")
settings = get_settings()

from bycco.service.paymentrequest import get_payment_requests, email_paymentrequest


async def main():
    """
    check if the fide id is filled in and if not   fetch it from API KBSB
    """

    # fetch all enrollments
    await connect_mongodb()
    prqs = (await get_payment_requests({
        "reason": "enrollment",
        "sentdate": None,
    })).paymentrequests
    for i,prq in enumerate(prqs):
            print(i+1, "sending pr for ", prq.first_name, prq.last_name)
            await email_paymentrequest(prq.id)

if __name__ == "__main__":
    asyncio.run(main())
