import asyncio
from fastapi import FastAPI
from pathlib import Path
from bycco.db.db_room import DbRoom
from csv import DictReader
from reddevil.core import (
    register_app,
    get_settings,
    connect_mongodb,
    close_mongodb,
    get_mongodb,
)

app = FastAPI(
    title="Bycco",
    description="BJK 2023",
    version="1",
)
register_app(app=app, settingsmodule="bycco.settings")
settings = get_settings()


rootpath = Path("..")
print("rootpath", rootpath.resolve())


class MongodbRoomWriter:
    async def __aenter__(self):
        await connect_mongodb()
        return self

    async def __aexit__(self, *args):
        await close_mongodb()

    async def write(self, rd: dict):
        await DbRoom.add(
            {
                "blocked": False,
                "capacity": int(rd["capacity"]),
                "enabled": True,
                "number": rd["roomnr"],
                "roomtype": rd["roomtype"],
            }
        )


class RoomCsvReader:
    def __enter__(self):
        self.fd = open(rootpath / "share" / "data" / "rooms.csv", "r")
        self.reader = DictReader(self.fd)
        return self.reader

    def __exit__(self, *args):
        self.fd.close()


async def main():
    async with MongodbRoomWriter() as writer:
        db = await get_mongodb()
        with RoomCsvReader() as reader:
            for record in reader:
                await writer.write(record)


if __name__ == "__main__":
    asyncio.run(main())
