# copyright Ruben Decrop 2012 - 2015
# copyright Chessdevil Consulting BVBA 2015 - 2019

# all database request come in here, but we do not use dataclasess
# if needed we define extra models an do marshalling functions at the service level
# All _id are translatesd to stringified id on output
# All functions expect stringified id as input

import pymongo
from reddevil.core import DbBase, get_mongodb, get_settings


class DbEnrollment(DbBase):
    COLLECTION = "enrollment"
    DOCUMENTTYPE = "Enrollment"
    VERSION = 1

    @classmethod
    async def maxSubsciptionNumber(cls) -> int:
        coll = get_mongodb()[cls.COLLECTION]
        cursor = coll.find(
            {},
            {"enrollmentnumber": 1},
            sort=[("enrollmentnumber", pymongo.DESCENDING)],
            limit=1,
        )
        docs = await cursor.to_list(1)
        return docs[0]["enrollmentnumber"] if len(docs) == 1 else 0

    @classmethod
    async def maxInvoiceNumber(cls) -> int:
        settings = get_settings()
        coll = get_mongodb()[cls.COLLECTION]
        cursor = coll.find(
            {},
            {"invoicenumber": 1},
            sort=[("invoicenumber", pymongo.DESCENDING)],
            limit=1,
        )
        docs = await cursor.to_list(1)
        maxinvoice = settings.INVOICENUMBERSTART - 1
        if len(docs):
            d = docs[0]
            nr = d.get("invoicenumber", None)
            if nr:
                maxinvoice = nr
        return maxinvoice
