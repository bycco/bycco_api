# copyright Ruben Decrop 2012 - 2015
# copyright Chessdevil Consulting BVBA 2015 - 2020

import logging
import datetime

from motor.motor_asyncio import AsyncIOMotorClient
from bycco.service.secrets import get_secret
from fastapi.exceptions import HTTPException
from sqlalchemy import create_engine
from sqlmodel import create_engine as sm_create_engine


log = logging.getLogger(__name__)

def date2datetime(d, f):
    """
    converts field f of input mongo document d from date to datetime
    as mongo does not supports dates
    """
    if f in d and isinstance(d[f], date):
        t = datetime.min.time()
        d[f] = datetime.combine(d[f], t)


# def get_mysql():
#     """
#     a singleton function to get the mongodb database
#     """
#     if not hasattr(get_mysql, "conn"):
#         mysqlparams = get_secret("mysql")
#         try:
#             conn = mc.connect(
#                 host=mysqlparams["dbhost"],
#                 user=mysqlparams["dbuser"],
#                 password=mysqlparams["dbpassword"],
#                 database=mysqlparams["dbname"],
#                 ssl_disabled=True,
#             )
#             setattr(get_mysql, "conn", conn)
#         except mc.Error as e:
#             log.error(f"Failed to set up Mysql connection: {e}")
#             raise HTTPException(status_code=503, detail="CannotConnectMysql")
#     return getattr(get_mysql, "conn")

def mysql_engine():
    """
    a singleton function returning a sqlalchemy engine for the mysql database
    """
    if not hasattr(mysql_engine, "engine"):
        mysqlparams = get_secret("mysql")
        host = mysqlparams["dbhost"]
        user = mysqlparams["dbuser"]
        password = mysqlparams["dbpassword"]
        dbname = mysqlparams["dbname"]
        url = f"mysql+pymysql://{user}:{password}@{host}/{dbname}"
        mysql_engine.engine = create_engine(
            url,
            pool_recycle=300,
            pool_pre_ping=True,
            connect_args={
                "ssl_disabled": True,
            },
        )
        mysql_engine.sm_engine = sm_create_engine(
            url,
            pool_recycle=300,
            pool_pre_ping=True,
            connect_args={
                "ssl_disabled": True,
            },
        )
    return mysql_engine.engine

def mysql_sm_engine():
    """
    a singleton function returning a sqlalchemy engine for the mysql database
    """
    if not hasattr(mysql_engine, "sm_engine"):
        mysql_engine()
    return mysql_engine.sm_engine

