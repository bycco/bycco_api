# copyright Ruben Decrop 2012 - 2015
# copyright Chessdevil Consulting BVBA 2015 - 2022

# all database request come in here, but we do not use dataclasess
# if needed we define extra models an do marshalling functions at the service level
# All _id are translatesd to stringified id on output
# All functions expect stringified id as input

from reddevil.core import DbBase, get_mongodb
from pymongo import ReturnDocument


class DbCounter(DbBase):
    COLLECTION = "counter"
    DOCUMENTTYPE = "Counter"
    VERSION = 1

    @classmethod
    async def next(cls, counter: str) -> int:
        """
        give the next value of a counter,
        returns 1 if the counter not yet exists
        """
        db = await get_mongodb()
        doc = await db.counter.find_one_and_update(
            {"_id": counter},
            {"$inc": {"value": 1}},
            return_document=ReturnDocument.AFTER,
            upsert=True,
        )
        return doc["value"]
