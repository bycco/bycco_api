# copyright Ruben Decrop 2012 - 2015
# copyright Chessdevil Consulting BVBA 2015 - 2019

import json
import asyncio
import logging
from pathlib import Path
from fastapi import HTTPException
from mysql.connector import connect, Error

from reddevil.core import get_settings
from reddevil.core.secrets import get_secret

log = logging.getLogger(__name__)


def get_mysqlconnection():
    if not hasattr(get_mysqlconnection, "conn"):
        settings = get_settings()
        mysqlparams = get_secret("mysql")
        try:
            conn = connect(
                host=mysqlparams["dbhost"],
                user=mysqlparams["dbuser"],
                password=mysqlparams["dbpassword"],
                database=mysqlparams["dbname"],
                ssl_disabled=True,
            )
            setattr(get_mysqlconnection, "conn", conn)
        except Error as e:
            log.error(f"Failed to set up Mysql connection: {e}")
            raise HTTPException(status_code=503, detail="CannotConnectMysql")
    return getattr(get_mysqlconnection, "conn")


class DbSignaletique:
    @classmethod
    def _getMember(cls, idbel):
        try:
            idbeln = int(idbel)
        except:
            raise HTTPException(status_code=400, detail="CannotConvertIdbelInt")
        with get_mysqlconnection().cursor() as c:
            c.execute("select * from signaletique where Matricule = %s", (idbeln,))
            return c.fetchone()

    @classmethod
    async def getMember(cls, idbel: str):
        def fn():
            return cls._getMember(idbel)

        loop = asyncio.get_event_loop()
        return await loop.run_in_executor(None, fn)
