# copyright Ruben Decrop 2012 - 2015
# copyright Chessdevil Consulting BVBA 2015 - 2019

# all database request come in here, but we do not use dataclasess
# if needed we define extra models an do marshalling functions at the service level
# All _id are translatesd to stringified id on output
# All functions expect stringified id as input

from reddevil.core import DbBase


class DbAttendee(DbBase):
    COLLECTION = "attendee"
    DOCUMENTTYPE = "Attendee"
    VERSION = 1
    SIMPLEFIELDS = [
        "birthyear",
        "category",
        "first_name",
        "gender",
        "last_name",
        "idbel",
        "idfide",
        "idclub",
        "locale",
        "meals",
        "nationalityfide",
        "rating",
    ]
    IDGENERATOR = "objectid"
    HIDDENFIELDS = ["badgeimage"]
