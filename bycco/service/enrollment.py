# copyright Ruben Decrop 2012 - 2015
# copyright Chessdevil Consulting BVBA 2015 - 2019

import logging

import io, csv
import pandas as pd
from typing import Dict, Any, cast
from datetime import datetime
from binascii import a2b_base64
from fastapi import BackgroundTasks
from fastapi.responses import Response
from jinja2 import FileSystemLoader, Environment


from bycco.service.playerlist import getBelplayer

from reddevil.core import encode_model
from bycco.models.mail import MailParams
from bycco.service.mail import sendEmailNoAttachments


# from bycco.service.mail import sendconfirmationmail

from bycco.models.md_enrollment import (
    CheckIdReply,
    Enrollment,
    EnrollmentCategory,
    EnrollmentEvent,
    EnrollmentIn,
    EnrollmentList,
    EnrollmentOut,
    EnrollmentUpdate,
    NatStatus,
)


from bycco.db.db_enrollment import DbEnrollment
from reddevil.core import RdNotFound, RdBadRequest, get_settings

log = logging.getLogger(__name__)
settings = get_settings()
env = Environment(loader=FileSystemLoader("bycco/templates"), trim_blocks=True)
current_event = EnrollmentEvent(
    title="BJK 2023",
    startdate="2023-02-19",
    enddate="2023-02-25",
    eventtype="Youth,Categories",
    options={"categories": [e.value for e in EnrollmentCategory], "agelimit": 2003},
)

### CRUD operations


async def add_enrollment(e: Enrollment) -> str:
    id = await DbEnrollment.add(e.dict(exclude_unset=True))
    return id


async def get_enrollment(id: str, options: dict = {}) -> Enrollment:
    """
    get the Enrollment
    """
    validator = options.pop("_class", Enrollment)
    filter = dict(id=id, **options)
    tdict = await DbEnrollment.find_single(filter)
    e = cast(Enrollment, encode_model(tdict, validator))
    log.info(f"get e {e.id}")
    return e


async def get_enrollments(options: dict = {}) -> EnrollmentList:
    """
    get the Enrollment
    """
    validator = options.pop("_class", Enrollment)
    if "_fieldlist" not in options and validator != Enrollment:
        options["_fieldlist"] = validator.__fields__.keys()
    docs = await DbEnrollment.find_multiple(options)
    subs = []
    for d in docs:
        try:
            subs.append(encode_model(d, validator))
        except Exception:
            log.info(f'issue with {d["last_name"]} {d["first_name"]}')
    return EnrollmentList(enrollments=subs)


async def update_enrollment(
    sub_id, su: EnrollmentUpdate, options: dict = {}
) -> Enrollment:
    """
    update a enrollment
    """
    validator = options.pop("_class", EnrollmentOut)
    sdict = await DbEnrollment.update(sub_id, su.dict(exclude_unset=True), options)
    so = cast(Enrollment, encode_model(sdict, validator))
    return so


### Application methods


def get_natstatus(nat_fide: str) -> NatStatus:
    if nat_fide == "BEL":
        return NatStatus.fidebelg
    if nat_fide:
        return NatStatus.nobelg
    return NatStatus.unknown


def natstatus2int(nat_fide: str) -> int:
    if nat_fide == "BEL":
        return 0
    if nat_fide:
        return 1
    return 2


async def create_enrollment(si: EnrollmentIn) -> str:
    """
    add a new enrollment
    """
    log.info("add enrollments")
    bp = await getBelplayer(si.idbel)
    if not bp:
        raise RdNotFound(description="BelplayerNotfound")
    sl = await get_enrollments(
        {
            "enrollmentevent.title": current_event.title,
            "idbel": si.idbel,
        }
    )
    log.info(f"found existing enrollment for {si.idbel}")
    if sl.enrollments:
        id = sl.enrollments[0].id
    else:
        id = await add_enrollment(
            Enrollment(
                idbel=si.idbel,
                badgelength=0,
                remarks="",
                rating=0,
                ratingbel=0,
                ratingfide=0,
            )
        )
    precat = "B" if bp.gender == "M" else "G"
    log.info(f"bp natstatus {bp.fide_nationality}")
    su = EnrollmentUpdate(
        birthyear=bp.birthyear,
        category=precat + si.category,
        emailattendant=si.emailattendant,
        emailparent=si.emailparent,
        emailplayer=si.emailplayer,
        first_name=bp.first_name,
        fullnameattendant=si.fullnameattendant,
        fullnameparent=si.fullnameparent,
        gender=bp.gender,
        idclub=bp.club_id,
        idfide=bp.fide_id,
        last_name=bp.last_name,
        locale=si.locale,
        mobileattendant=si.mobileattendant,
        mobileparent=si.mobileparent,
        mobileplayer=si.mobileplayer,
        nationalitybel=bp.nationality,
        nationalityfide=bp.fide_nationality,
        natstatus=bp.fide_nationality,
        enrollmentevent=current_event,
    )
    await update_enrollment(id, su)
    log.info(f"created {id}")
    return id


async def check_id(idbel: str) -> CheckIdReply:
    """
    fill in
    """
    bp = None
    try:
        bp = await getBelplayer(idbel)
        log.info(f"belplayer {bp}")
        if not bp:
            return CheckIdReply(belfound=False)
    except Exception as e:
        log.info(f"exception raised {bp} {e}")
        return CheckIdReply(belfound=False)
    log.info(f"bp: {bp.fide_nationality} {get_natstatus(bp.fide_nationality)}")
    cr = CheckIdReply(
        belfound=True,
        birthyear=bp.birthyear,
        first_name=bp.first_name,
        gender=bp.gender,
        idbel=idbel,
        idclub=str(bp.club_id),
        idfide=str(bp.fide_id),
        last_name=bp.last_name,
        nationalitybel=bp.nationality,
        nationalityfide=bp.fide_nationality,
        natstatus=get_natstatus(bp.fide_nationality),
    )
    if cr.birthyear < current_event.options["agelimit"]:
        cr.age_ok = False
        return cr
    sl = await get_enrollments({"event.title": current_event.title, "idbel": idbel})
    if sl.enrollments:
        sub = sl.enrollments[0]
        cr.subconfirmed = sub.confirmed
        cr.subid = sub.id
    return cr


async def xls_enrollments() -> bytes:
    """
    get all enrollments in xls format
    """
    docs = await DbEnrollment.find_multiple(
        {"_fieldlist": EnrollmentOut.__fields__.keys()}
    )
    df = pd.DataFrame.from_records(docs)
    log.info(f"df: {df}")
    bio = io.BytesIO()
    with pd.ExcelWriter(bio) as writer:
        df.to_excel(writer, sheet_name="Enrollments")
    b = bio.getvalue()
    log.info(f"len {len(b)}")
    return b


async def get_photo(idbel: str):
    photo = await DbEnrollment.find_single(
        {
            "idbel": idbel,
            "_fieldlist": ["badgeimage", "badgemimetype"],
        }
    )
    return Response(content=photo["badgeimage"], media_type=photo["badgemimetype"])


async def upload_photo(id: str, photo: str) -> None:
    try:
        header, data = photo.split(",")
        imagedata = a2b_base64(data)
        su = EnrollmentUpdate(
            badgemimetype=header.split(":")[1].split(";")[0],
            badgeimage=imagedata,
            badgelength=len(cast(str, imagedata)),
        )
    except:
        raise RdBadRequest(description="BadPhotoData")
    await update_enrollment(id, su)


async def confirm_enrollment(id: str, bt: BackgroundTasks) -> None:
    su = EnrollmentUpdate(confirmed=True, registrationtime=datetime.now(), enabled=True)
    enr = await update_enrollment(id, su)
    email_enrollment(enr)
    # prqid = await create_pr_enrollment(id)
    # await email_pr_enrollment(prqid)


async def disable_enrollment(id: str) -> None:
    await update_enrollment(id, EnrollmentUpdate(enabled=False))


def email_enrollment(enr: Enrollment) -> None:
    assert enr.locale and enr.nationalityfide
    emails = []
    for em in [enr.emailplayer, enr.emailparent, enr.emailattendant]:
        if em:
            emails.append(em)
    mp = MailParams(
        subject="BJK - CBj - BJLM - BYCC 2023",
        sender=settings.EMAIL["sender"],
        receiver=",".join(emails),
        template="mailenrollment_{locale}.md",
        locale=enr.locale,
        attachments=[],
        bcc=settings.EMAIL["bcc_enrollment"],
    )
    edict = enr.dict()
    edict["gender"] = edict["gender"].value
    edict["category"] = edict["category"].value
    edict["natstatus"] = natstatus2int(enr.nationalityfide)
    sendEmailNoAttachments(mp, edict, "confirmation enrollment")


async def getBadges(cat: str):
    """
    get the Badges
    """
    log.info(f"badges cat {cat}")
    docs = (await get_enrollments({"category": cat})).enrollments
    pages = []
    badges = []
    j = 0
    sorteddocs = sorted(docs, key=lambda x: f"{x.last_name}, {x.first_name}")
    log.info(f"nr docs {len(docs)}")
    for ix, p in enumerate(sorteddocs):
        rix = j % 2 + 1
        cix = j // 2 + 1
        badge = {
            "first_name": p.first_name,
            "last_name": p.last_name,
            "rating": max(p.ratingbel or 0, p.ratingfide or 0),
            "chesstitle": "",
            "category": p.category.value,
            "meals": p.meals or "",
            "mealsclass": "badge_{}".format(p.meals or "NM"),
            "photourl": f"/photo/{p.id}",
            "positionclass": "badge{0}{1}".format(cix, rix),
            "ix": ix,
        }
        # log.info(f"badge: {badge}")
        badges.append(badge)
        j += 1
        if j == 8:
            j = 0
            pages.append(badges)
            badges = []
    if j > 0:
        pages.append(badges)
    tmpl = env.get_template("printbadge.j2")
    return tmpl.render({"pages": pages})


async def getBadgeEters():
    """
    get the Badges
    """
    docs = (await get_enrollments()).attendees
    docs = [d for d in docs if d.meals and d.meals in ["FB", "HB"]]
    log.info(f"{len(docs)} eters")
    pages = []
    badges = []
    j = 0
    sorteddocs = sorted(docs, key=lambda x: f"{x.last_name}, {x.first_name}")
    for ix, p in enumerate(sorteddocs):
        rix = j % 2 + 1
        cix = j // 2 + 1
        badge = {
            "first_name": p.first_name,
            "last_name": p.last_name,
            "rating": max(p.ratingbel or 0, p.ratingfide or 0),
            "chesstitle": p.chesstitle + " " if p.chesstitle else "",
            "category": p.category,
            "meals": p.meals or "",
            "mealsclass": "badge_{}".format(p.meals or "NM"),
            "photourl": f"/photo/{p.id}",
            "positionclass": "badge{0}{1}".format(cix, rix),
            "ix": ix,
        }
        # log.info(f"badge: {badge}")
        badges.append(badge)
        j += 1
        if j == 8:
            j = 0
            pages.append(badges)
            badges = []
    if j > 0:
        pages.append(badges)
    tmpl = env.get_template("printbadge.j2")
    return tmpl.render({"pages": pages})


async def get_namecards(cat: str):
    """
    get the Namecards
    """
    filter: Dict[str, Any] = {"enabled": True}
    filter["category"] = {"$in": [f"B{cat[1:]}", f"G{cat[1:]}"]}
    # if cat in ("U10", "U18", "U20"):
    #     filter["category"] = {"$in": [f"B{cat[1:]}", f"G{cat[1:]}"]}
    # else:
    #     filter["category"] = cat
    log.info(f"filter {filter}")
    docs = (await get_enrollments(filter)).enrollments
    log.info(f"nr docs {len(docs)}")
    pages = []
    cards = []
    j = 0
    sorteddocs = sorted(docs, key=lambda x: f"{x.last_name}, {x.first_name}")
    for ix, p in enumerate(sorteddocs):
        rix = j % 2 + 1
        ct = ""
        # ct = p.chesstitle + " " if p.chesstitle else ""
        card = {
            "fullname": "{0}{1} {2}".format(ct, p.last_name, p.first_name),
            "natrating": p.ratingbel or 0,
            "fiderating": p.ratingfide or 0,
            "category": p.category.value,
            "locale": p.locale,
            # 'photourl': '/photo/{0}'.format(p.id),
            "positionclass": "card_1{0}".format(rix),
            "ix": ix,
        }
        cards.append(card)
        j += 1
        if j == 2:
            j = 0
            pages.append(cards)
            cards = []
    if j > 0:
        pages.append(cards)
    tmpl = env.get_template("printnamecard.j2")
    return tmpl.render({"pages": pages})


async def get_namecards_ids(ids: list):
    """
    get the Namecards for ids
    """
    filter: Dict[str, Any] = {"enabled": True}
    filter["idbel"] = {"$in": ids}
    log.debug(f"filter {filter}")
    docs = (await get_enrollments(filter)).enrollments
    log.info(f"nr docs {len(docs)}")
    pages = []
    cards = []
    j = 0
    sorteddocs = sorted(docs, key=lambda x: f"{x.last_name}, {x.first_name}")
    for ix, p in enumerate(sorteddocs):
        rix = j % 2 + 1
        ct = ""
        # ct = p.chesstitle + " " if p.chesstitle else ""
        card = {
            "fullname": "{0}{1} {2}".format(ct, p.last_name, p.first_name),
            "natrating": p.ratingbel or 0,
            "fiderating": p.ratingfide or 0,
            "category": p.category.value,
            "locale": p.locale,
            # 'photourl': '/photo/{0}'.format(p.id),
            "positionclass": "card_1{0}".format(rix),
            "ix": ix,
        }
        cards.append(card)
        j += 1
        if j == 2:
            j = 0
            pages.append(cards)
            cards = []
    if j > 0:
        pages.append(cards)
    tmpl = env.get_template("printnamecard.j2")
    return tmpl.render({"pages": pages})
