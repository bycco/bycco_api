#    Copyright 2021 Chessdevil Consulting
import logging
import json
import yaml
from pathlib import Path
from typing import Any
from google.cloud import secretmanager
from reddevil.core import RdInternalServerError, get_settings


log = logging.getLogger(__name__)


def secretmanager_client():
    if not hasattr(secretmanager_client, "sc"):
        setattr(secretmanager_client, "sc", secretmanager.SecretManagerServiceClient())
    return secretmanager_client.sc


def get_secret(name: str) -> Any:
    settings = get_settings()
    project = settings.GOOGLE_PROJECT_ID
    sconfig = settings.SECRETS.get(name, None)
    if not sconfig:
        log.error(f"Secret {name} not configured")
        raise RdInternalServerError(description="SecretNotConfigured")
    sname = sconfig.get("name", name)
    manager = sconfig.get("manager", "filejson")
    log.info(f"fecthing secret {sname} using manager {manager}")
    if manager == "googlejson":
        version = sconfig.get("version", "latest")
        try:
            reply = secretmanager_client().access_secret_version(
                request={
                    "name": f"projects/{project}/secrets/{sname}/versions/{version}"
                }
            )
        except:
            log.exception("Could not get secret")
        return json.loads(reply.payload.data)
    if manager == "googleyaml":
        version = sconfig.get("version", "latest")
        try:
            reply = secretmanager_client().access_secret_version(
                request={
                    "name": f"projects/{project}/secrets/{sname}/versions/{version}"
                }
            )
        except:
            log.exception("Could not get secret")
        return yaml.safe_load(reply.payload.data)
    if manager == "filejson":
        extension = sconfig.get("extension", ".json")
        sfile = Path(settings.SECRETS_PATH) / f"{sname}{extension}"
        with open(sfile) as f:
            return json.load(f)
    if manager == "fileyaml":
        extension = sconfig.get("extension", ".yaml")
        sfile = Path(settings.SECRETS_PATH) / f"{sname}{extension}"
        with open(sfile) as f:
            return yaml.safe_load(f)
