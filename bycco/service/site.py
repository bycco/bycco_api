# copyright Ruben Decrop 2012 - 2015
# copyright Chessdevil Consulting BVBA 2015 - 2019

import logging
from select import select
import yaml
import os
import csv
from pathlib import Path
from jinja2 import Template, FileSystemLoader, Environment
from typing import Optional
from google.oauth2 import service_account
from googleapiclient.discovery import build
from googleapiclient.http import MediaIoBaseDownload
from reddevil.core import get_settings
from bycco.service.secrets import get_secret

log = logging.getLogger(__name__)
settings = get_settings()
rootdir = Path("..")

# google drive API
scopes = ["https://www.googleapis.com/auth/drive"]
_service = None


def get_drive_service():
    """
    get Google drive service with delegated credentials of me myself and I
    this is dangerous, needs imrpovement
    """
    if not hasattr(get_drive_service, "service"):
        secret = get_secret("gmail")
        cr = service_account.Credentials.from_service_account_info(
            secret, scopes=["https://www.googleapis.com/auth/drive"]
        )
        delegated_credentials = cr.with_subject("ruben.decrop@bycco.be")
        setattr(
            get_drive_service,
            "service",
            build("drive", "v3", credentials=delegated_credentials),
        )
    return get_drive_service.service


async def fetchI18n() -> None:
    request = (
        get_drive_service()
        .files()
        .export_media(fileId=settings.GOOGLEDRIVE_I18N_ID, mimeType="text/csv")
    )
    with (rootdir / "bycco_shared" / "data" / "i18n.csv").open("wb") as fh:
        downloader = MediaIoBaseDownload(fh, request)
        done = False
        while done is False:
            status, done = downloader.next_chunk()
    with (rootdir / "bycco_shared" / "data" / "i18n.csv").open(newline="") as csvfile:
        reader = csv.DictReader(csvfile)
        allrows = []
        for r in reader:
            allrows.append(r)
    for l in ["en", "fr", "nl", "de"]:
        with (rootdir / "bycco_ui" / "src" / "lang" / f"{l}.js").open(
            "w", encoding="utf8"
        ) as f:
            f.write("export default {\n")
            for r in allrows:
                f.write(f'"{r["key"]}": `{r[l]}`,\n')
            f.write("}\n")
