# copyright Ruben Decrop 2012 - 2015
# copyright Chessdevil Consulting BVBA 2015 - 2019

import logging

import io, csv
from typing import cast

from reddevil.core import encode_model, get_settings
from bycco.db.db_room import DbRoom
from bycco.models.md_room import (
    Room,
    RoomList,
    RoomOutForList,
)
from reddevil.core import RdBadRequest

log = logging.getLogger(__name__)
settings = get_settings()
roomtypes = settings.COMMON.get("roomtypes")


async def get_room(id: str, options: dict = {}) -> Room:
    """
    get the Room
    """
    validator = options.pop("_class", Room)
    filter = dict(id=id, **options)
    tdict = await DbRoom.find_single(filter)
    return cast(Room, encode_model(tdict, validator))


async def get_room_number(number: str, options: dict = {}) -> Room:
    """
    get room by number
    """
    validator = options.pop("_class", Room)
    filter = dict(number=number, **options)
    tdict = await DbRoom.find_single(filter)
    return cast(Room, encode_model(tdict, validator))


async def get_rooms(options: dict = {}) -> RoomList:
    """
    get all Rooms
    """
    validator = options.pop("_class", Room)
    if "_fieldlist" not in options and validator != Room:
        options["_fieldlist"] = validator.__fields__.keys()
    log.info(f"calling multiple with {options}")
    docs = await DbRoom.find_multiple(options)
    rooms = [encode_model(d, validator) for d in docs]
    return RoomList(rooms=rooms)


async def update_room(room_id, ru: Room, options: dict = {}) -> Room:
    """
    update a enrollment
    """
    validator = options.pop("_class", Room)
    updict = await DbRoom.update(room_id, ru.dict(exclude_unset=True), options)
    return cast(Room, encode_model(updict, validator))


async def get_free_rooms(roomtype: str) -> RoomList:
    """
    update a enrollment
    """
    if roomtype not in roomtypes:
        log.error(f"Unknown roomtype {roomtype}")
        raise RdBadRequest()
    rooms = await get_rooms(
        {
            "roomtype": roomtype,
            "blocked": False,
            "enabled": True,
            "reservation_id": None,
            "_class": RoomOutForList,
        }
    )
    return rooms


async def get_csv_rooms() -> str:
    """
    get all rooms in csv format
    """
    fieldnames = list(Room.__fields__.keys())
    csvstr = io.StringIO()
    csvf = csv.DictWriter(csvstr, fieldnames)
    csvf.writeheader()
    rooms = (await getRooms()).rooms
    for room in rooms:
        csvf.writerow(room.dict())
    return csvstr.getvalue()


async def importCsvRooms(csvstr: str) -> None:
    """
    get all rooms in csv format
    """
    csvf = io.StringIO(csvstr)
    reader = csv.DictReader(csvf, delimiter=",", quoting=csv.QUOTE_ALL)
    for row in reader:
        await DbRoom.add(
            {
                "blocked": False,
                "capacity": row["capacity"],
                "enabled": True,
                "number": row["roomnr"],
                "roomtype": row["roomtype"],
            }
        )
