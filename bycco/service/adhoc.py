# copyright Ruben Decrop 2012 - 2015
# copyright Chessdevil Consulting BVBA 2015 - 2019

import logging
import uuid
from pathlib import Path
from jinja2 import FileSystemLoader, Environment
from markdown2 import Markdown
from bycco.service.enrollment import get_enrollments, update_enrollment
from bycco.models.md_enrollment import EnrollmentUpdate
from bycco.db.db_attendee import DbAttendee
from bycco.db.mysql_signaletique import get_mysqlconnection
from reddevil.core import get_settings


log = logging.getLogger(__name__)
rootpath = Path("..")

md = Markdown(extras=["tables"])
env = Environment(loader=FileSystemLoader("./bycco/templates"), trim_blocks=True)


# async def reservation_clean():
#     from bycco.db import get_mongodb

#     db = await get_mongodb()
#     coll = db.by_reservation
#     cursor = coll.find(
#         {},
#         projection={
#             "first_name": 1,
#             "last_name": 1,
#             "_creationtime": 1,
#             "number": 1,
#         },
#         sort=[("_creationtime", 1)],
#     )
#     async for doc in cursor:
#         if not doc.get("number"):
#             coll.update_one(
#                 {"_id": doc["_id"]},
#                 {"$set": {"number": await DbCounter.next("reservation_number")}},
#             )
#         if not doc.get("assignments"):
#             coll.update_one(
#                 {"_id": doc["_id"]},
#                 {"$set": {"assignments": []}},
#             )
#     cursor.rewind()
#     return await cursor.to_list(None)


# async def read_rooms():
#     from bycco.service.room import importCsvRooms

#     with open(rootpath / "share" / "data" / "kamers.csv") as f:
#         kamers = f.read()
#     await importCsvRooms(kamers)


async def update_elo():
    log.info("updating elo")
    enrs = await get_enrollments()
    conn = get_mysqlconnection()
    for ix, e in enumerate(enrs.enrollments):
        if e is None:
            continue
        with conn.cursor(buffered=True) as c:
            c.execute(
                "select Elo, Fide from p_player202301 where Matricule = %s", (e.idbel,)
            )
            (ratingbel, idfide) = c.fetchone()
            ratingbel = ratingbel or 0
            ratingfide = 0
            if idfide != 0:
                c.execute("select ELO from fide202302 where ID_NUMBER = %s", (idfide,))
                row = c.fetchone()
                ratingfide = row[0] or 0 if row is not None else 0
        log.info(f"{ix} {e.first_name} {e.last_name} {ratingbel} {ratingfide}")
        await update_enrollment(
            e.id,
            EnrollmentUpdate(ratingbel=ratingbel, ratingfide=ratingfide, idfide=idfide),
        )
    return "OK"


async def enrollment2attendee():
    enrs = await get_enrollments()
    log.info("enrollment 2 attendee")
    for ix, e in enumerate(enrs.enrollments):
        if not e.enabled:
            continue
        att = e.dict()
        att["enr_id"] = att.pop("id")
        att["meals"] = "NO"
        await DbAttendee.add(att)
        log.info(f"{ix} {e.first_name} {e.last_name}")


# async def rsv2eaters():
#     rsvs = (await get_reservations()).reservations
#     for rsv in rsvs:
#         if not rsv.enabled:
#             continue
#         if rsv.meals not in ["half", "full"]:
#             continue
#         if rsv.last_name == "Bycco":
#             continue
#         for g in rsv.guestlist:
#             if g.player:
#                 continue
#             await add_attendee(
#                 AttendeeIn(
#                     first_name=g.first_name,
#                     last_name=g.last_name,
#                     category="EAT",
#                     meals="HALF" if rsv.meals == "half" else "FULL",
#                 )
#             )


async def print_prizes(cat):
    settings = get_settings()
    prizes = settings.PRIZES.get(cat)
    log.info(f"PRIZES {settings.PRIZES}")
    log.info(f"prizes {cat} {prizes}")
    cards = []
    pages = []
    j = 0
    for n, place in enumerate(prizes):
        idbel = place.get("idbel")
        if idbel == 0:
            continue
        enrs = (await get_enrollments({"idbel": str(idbel)})).enrollments
        if not enrs:
            log.error(f"cannot find player with idbel {idbel}")
            continue
        enr = enrs[0]
        rix = j % 3 + 1
        name = f"{enr.first_name} {enr.last_name}"
        code = uuid.uuid4()
        print(f"{name},{code}")
        p = {
            "name": f"{enr.first_name} {enr.last_name}",
            "positionclass": f"prize_1{rix}",
            "category": enr.category.value,
            "place": n + 1,
            "prize": place["prize"],
            "code": code,
            "gender": "M" if enr.category[0] == "B" else "F",
        }
        j += 1
        cards.append(p)
        if j == 3:
            j = 0
            pages.append(cards)
            cards = []
    if cards:
        pages.append(cards)
    tmpl = env.get_template("printprize.j2")
    return tmpl.render({"pages": pages})
