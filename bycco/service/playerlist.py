# copyright Ruben Decrop 2012 - 2015
# copyright Chessdevil Consulting BVBA 2015 - 2019

import logging

from typing import Optional
from bycco.models.md_playerlist import Belplayer
from reddevil.core import RdBadRequest, RdNotFound
from httpx import (
    TransportError,
    DecodingError,
    AsyncClient,
)

log = logging.getLogger(__name__)

url_bel = "https://api.frbe-kbsb-ksb.be/v1/player/{id}"


async def getBelplayer(id: str) -> Optional[Belplayer]:
    """
    get the player
    """
    url = url_bel.format(id=id)
    try:
        async with AsyncClient() as client:
            rc = await client.get(url)
            plyrs = rc.json()
    except DecodingError:
        log.error(f"decoding error on {url}")
        raise RdBadRequest("DecodingErrorKBSB")
    except TransportError:
        log.error(f"Request error to {url}")
        raise RdBadRequest("RequestErrorKBSB")
    log.debug(f"plyrs {plyrs}")
    return Belplayer(**plyrs[0]) if plyrs else None
