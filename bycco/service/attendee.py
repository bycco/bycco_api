# copyright Ruben Decrop 2012 - 2015
# copyright Chessdevil Consulting BVBA 2015 - 2019

import logging

from typing import List, Optional, Dict, Any, cast
from datetime import date, datetime, timezone
from binascii import a2b_base64
from jinja2 import FileSystemLoader, Environment
from fastapi.responses import Response


from reddevil.core import encode_model


# from bycco.service.mail import sendconfirmationmail

from bycco.models.md_attendee import (
    Attendee,
    AttendeeIn,
    AttendeeUpdate,
    AttendeeList,
)


from bycco.db.db_attendee import DbAttendee
from reddevil.core import RdNotFound, RdBadRequest, get_settings

log = logging.getLogger(__name__)
settings = get_settings()
env = Environment(loader=FileSystemLoader("bycco/templates"), trim_blocks=True)


### CRUD operations


async def add_attendee(e: AttendeeIn) -> str:
    id = await DbAttendee.add(e.dict(exclude_unset=True))
    return str(id)


async def get_attendee(id: str, options: dict = {}) -> Attendee:
    """
    get the Attendee
    """
    validator = options.pop("_class", Attendee)
    filter = dict(id=id, **options)
    tdict = await DbAttendee.find_single(filter)
    e = cast(Attendee, encode_model(tdict, validator))
    log.info(f"get e {e.id}")
    return e


async def get_attendees(options: dict = {}) -> AttendeeList:
    """
    get the Attendee
    """
    validator = options.pop("_class", Attendee)
    category = options.pop("cat", None)
    if category:
        options["category"] = category
    ss = options.pop("ss", None)
    if "_fieldlist" not in options and validator != Attendee:
        options["_fieldlist"] = validator.__fields__.keys()
    docs = await DbAttendee.find_multiple(options)
    log.info(f"docs {len(docs)}")
    subs = [encode_model(d, validator) for d in docs]
    return AttendeeList(attendees=subs)


async def update_attendee(sub_id, su: AttendeeUpdate, options: dict = {}) -> Attendee:
    """
    update a attendee
    """
    validator = options.pop("_class", Attendee)
    sdict = await DbAttendee.update(sub_id, su.dict(exclude_unset=True), options)
    so = cast(Attendee, encode_model(sdict, validator))
    return so


async def delete_attendee(id: str) -> None:
    """
    delete an attendee
    """
    upd = await DbAttendee.delete(id)


# business


async def getBadgeEters():
    """
    get the Badges
    """
    docs = (await get_attendees()).attendees
    # docs = [d for d in docs if d.meals and d.meals in ["FULL", "HALF"]]
    log.info(f"{len(docs)} eters")
    pages = []
    badges = []
    j = 0
    sorteddocs = sorted(docs, key=lambda x: f"{x.last_name}, {x.first_name}")
    for ix, p in enumerate(sorteddocs):
        rix = j % 2 + 1
        cix = j // 2 + 1
        badge = {
            "first_name": p.first_name,
            "last_name": p.last_name,
            "rating": max(p.ratingbel or 0, p.ratingfide or 0),
            "chesstitle": p.chesstitle + " " if p.chesstitle else "",
            "category": p.category,
            "meals": p.meals or "",
            "mealsclass": "badge_{}".format(p.meals or "NM"),
            "photourl": f"/photo/{p.id}",
            "positionclass": "badge{0}{1}".format(cix, rix),
            "ix": ix,
        }
        # log.info(f"badge: {badge}")
        badges.append(badge)
        j += 1
        if j == 8:
            j = 0
            pages.append(badges)
            badges = []
    if j > 0:
        pages.append(badges)
    tmpl = env.get_template("printbadge.j2")
    return tmpl.render({"pages": pages})


async def get_badges(cat: str = None):
    """
    get the Badges
    """
    filter: Dict[str, Any] = {}
    if cat == "org":
        filter["category"] = {"$in": ["ORG", "ARB"]}
    elif cat is not None:
        filter["category"] = cat
    if filter:
        docs = (await get_attendees(filter)).attendees
    else:
        docs = (await get_attendees()).attendees
    log.info(f"docs {len(docs)}")
    pages = []
    badges = []
    j = 0
    sorteddocs = sorted(docs, key=lambda x: f"{x.last_name}, {x.first_name}")
    for ix, p in enumerate(sorteddocs):
        rix = j % 2 + 1
        cix = j // 2 + 1
        badge = {
            "first_name": p.first_name,
            "last_name": p.last_name,
            "category": p.category,
            "meals": p.meals or "",
            "mealsclass": "badge_{}".format(p.meals or "NO"),
            "photourl": f"/api/v1/attendee/{p.id}/photo",
            "positionclass": "badge{0}{1}".format(cix, rix),
            "ix": ix,
        }
        # log.info(f"badge: {badge}")
        badges.append(badge)
        j += 1
        if j == 8:
            j = 0
            pages.append(badges)
            badges = []
    if j > 0:
        pages.append(badges)
    tmpl = env.get_template("printbadge.j2")
    return tmpl.render({"pages": pages})


async def get_badges_ids(ids):
    """
    get the Badges
    """
    filter = {"idbel": {"$in": ids}}
    docs = (await get_attendees(filter)).attendees
    log.info(f"docs {len(docs)}")
    pages = []
    badges = []
    j = 0
    sorteddocs = sorted(docs, key=lambda x: f"{x.last_name}, {x.first_name}")
    for ix, p in enumerate(sorteddocs):
        rix = j % 2 + 1
        cix = j // 2 + 1
        badge = {
            "first_name": p.first_name,
            "last_name": p.last_name,
            "category": p.category,
            "meals": p.meals or "",
            "mealsclass": "badge_{}".format(p.meals or "NO"),
            "photourl": f"/api/v1/attendee/{p.id}/photo",
            "positionclass": "badge{0}{1}".format(cix, rix),
            "ix": ix,
        }
        # log.info(f"badge: {badge}")
        badges.append(badge)
        j += 1
        if j == 8:
            j = 0
            pages.append(badges)
            badges = []
    if j > 0:
        pages.append(badges)
    tmpl = env.get_template("printbadge.j2")
    return tmpl.render({"pages": pages})


async def get_photo(id: str):
    photo = await DbAttendee.find_single(
        {
            "id": id,
            "_fieldlist": ["badgeimage", "badgemimetype"],
        }
    )
    return Response(content=photo["badgeimage"], media_type=photo["badgemimetype"])


async def upload_photo(id: str, photo: str) -> None:
    try:
        header, data = photo.split(",")
        imagedata = a2b_base64(data)
        su = AttendeeUpdate(
            badgemimetype=header.split(":")[1].split(";")[0],
            badgeimage=imagedata,
            badgelength=len(cast(str, imagedata)),
        )
    except:
        raise RdBadRequest(description="BadPhotoData")
    await update_attendee(id, su)
