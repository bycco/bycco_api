# copyright Ruben Decrop 2012 - 2015
# copyright Chessdevil Consulting BVBA 2015 - 2019

import logging
from markdown2 import Markdown
from jinja2 import FileSystemLoader, Environment
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
from io import BytesIO
from pathlib import Path
from weasyprint import HTML, CSS
from reddevil.core import get_settings
from bycco.models.md_enrollment import Enrollment
from bycco.models.md_reservation import Reservation
from bycco.models.mail import MailParams

# from bycco.i18n import locale_msg
from bycco.service.mailbackend import backends

log = logging.getLogger(__name__)
settings = get_settings()
md = Markdown(extras=["tables"])


markdownstyle = """
<style>
th, td {
    padding: 8px;
    border: 1px solid #ddd;
}

table {
    border-collapse: collapse;
}

h1:after {
    content: ' ';
    display: block;
    border: 1px solid #aaa;
    margin-bottom: 1em;
}

ul, ol, h2, h3 {
    margin-bottom: 0.5em;
}
</style>
"""


def getCss():
    settings = get_settings()
    if not hasattr(getCss, "css"):
        with open(Path(settings.TEMPLATES_PATH) / "markdown.css") as f:
            setattr(getCss, "css", f.read())
    return getattr(getCss, "css")


def test_mail():
    """
    send a test mail
    """
    settings = get_settings
    log.info("sending testmail email")
    try:
        sender = settings.EMAIL["sender"]
        receiver = "ruben.decrop@gmail.com"
        msg = MIMEMultipart("related")
        msg["Subject"] = "Testmail 2"
        msg["From"] = sender
        msg["To"] = receiver
        msg.preamble = "This is a multi-part message in MIME format."
        msgAlternative = MIMEMultipart("alternative")
        msgText = MIMEText("Hi it is I Leclercq, I am in disguise")
        msgAlternative.attach(msgText)
        msgText = MIMEText("Hi, It is I <b>Leclercq</b> I am in disguise", "html")
        msgAlternative.attach(msgText)
        msg.attach(msgAlternative)
        mailbackend = backends[settings.EMAIL["backend"]]()
        mailbackend.send_message(msg)
        log.info(f"testmail sent for {receiver}")
    except Exception:
        log.exception("failed")


def sendReservationEmail(rsv: Reservation):
    settings = get_settings()
    env = Environment(loader=FileSystemLoader(settings.TEMPLATES_PATH), trim_blocks=True)
    i18n = settings.COMMON["i18n"]
    log.info("sending reservation email")
    tmpl = env.get_template(f"mailreservation_{rsv.locale}.md")
    context = rsv.dict()
    # translate
    context["lodging"] = i18n[context["lodging"]][context["locale"]]
    context["meals"] = i18n[context["meals"]][context["locale"]]
    # render the content using jinja2 templating giving markdown text
    markdowntext = tmpl.render(**context)
    # convert markdown text to html
    htmltext = f"{markdownstyle} {md.convert(markdowntext)}"
    try:
        sender = settings.EMAIL["sender"]
        receiver = rsv.email
        msg = MIMEMultipart("related")
        msg["Subject"] = "Floreal 2023"
        msg["From"] = sender
        msg["To"] = receiver
        if settings.EMAIL.get("bcc_reservation"):
            msg["Bcc"] = settings.EMAIL["bcc_reservation"]
        msg.preamble = "This is a multi-part message in MIME format."
        msgAlternative = MIMEMultipart("alternative")
        msgText = MIMEText(markdowntext)
        msgAlternative.attach(msgText)
        msgText = MIMEText(htmltext, "html")
        msgAlternative.attach(msgText)
        msg.attach(msgAlternative)
        backend = backends[settings.EMAIL["backend"]]()
        backend.send_message(msg)
        log.info(f"reservation email sent to {receiver}")
    except Exception:
        log.exception("failed")


def sendEmailNoAttachments(mp: MailParams, context: dict, name: str = ""):
    settings = get_settings()
    env = Environment(loader=FileSystemLoader(settings.TEMPLATES_PATH), trim_blocks=True)
    i18n = settings.COMMON["i18n"]
    tmpl = env.get_template(mp.template.format(locale=context["locale"]))
    markdowntext = tmpl.render(**context)
    htmltext = f"{markdownstyle} {md.convert(markdowntext)}"
    try:
        msg = MIMEMultipart("related")
        msg["Subject"] = mp.subject
        msg["From"] = mp.sender
        msg["To"] = mp.receiver
        if mp.bcc:
            msg["Bcc"] = mp.bcc
        msg.preamble = "This is a multi-part message in MIME format."
        msgAlternative = MIMEMultipart("alternative")
        msgText = MIMEText(markdowntext)
        msgAlternative.attach(msgText)
        msgText = MIMEText(htmltext, "html")
        msgAlternative.attach(msgText)
        msg.attach(msgAlternative)
        backend = backends[settings.EMAIL["backend"]]()
        backend.send_message(msg)
        log.info(f"email {name} sent to {mp.receiver}")
    except Exception:
        log.exception("failed")


def sendEmailWithPdf(mp: MailParams, context: dict, name: str = ""):
    settings = get_settings()
    env = Environment(loader=FileSystemLoader(settings.TEMPLATES_PATH), trim_blocks=True)
    i18n = settings.COMMON["i18n"]
    tmpl = env.get_template(mp.template.format(locale=context["locale"]))
    markdowntext = tmpl.render(**context)
    htmltext = md.convert(markdowntext)
    try:
        # receiver = rsv.email
        msg = MIMEMultipart("related")
        msg["Subject"] = mp.subject
        msg["From"] = mp.sender
        msg["To"] = mp.receiver
        if mp.bcc:
            msg["Bcc"] = mp.bcc
        msg.preamble = "This is a multi-part message in MIME format."
        msgAlternative = MIMEMultipart("alternative")
        msgText = MIMEText(markdowntext)
        msgAlternative.attach(msgText)
        msgText = MIMEText(htmltext, "html")
        msgAlternative.attach(msgText)
        msg.attach(msgAlternative)
        # pdf
        att = mp.attachments[0]
        assert att.template is not None
        pdftmpl = env.get_template(att.template.format(locale=context["locale"]))
        pdfmd = pdftmpl.render(**context)
        pdfhtml = md.convert(pdfmd)
        pdfio = BytesIO()
        HTML(string=pdfhtml).write_pdf(pdfio, stylesheets=[CSS(string=getCss())])
        msgAtt = MIMEBase("application", "octet-stream")
        msgAtt.set_payload(pdfio.getvalue())
        encoders.encode_base64(msgAtt)
        msgAtt.add_header(
            "Content-Disposition",
            f"attachment; filename={att.filename}",
        )
        msg.attach(msgAtt)
        backend = backends[settings.EMAIL["backend"]]()
        backend.send_message(msg)
        log.info(f"paymentrequest email sent to {mp.receiver}")
    except Exception:
        log.exception("send_mesfailed")


def sendcEnrollmentMail(s: Enrollment):
    """
    send confirmation email
    :param s: the Enrollment
    :return: None
    """
    # sub = {
    #     'fullname': f"{s.first_name} {s.last_name}",
    #     'birthdate': s.birthdate,
    #     'idclub': s.idclub,
    #     'nationality': s.nationality,
    #     'ratingbel': s.ratingbel,
    #     'ratingfide': s.ratingfide,
    #     'category': s.category,
    #     'paymessage': s.paymessage,
    # }
    # assert s.locale
    # # i18n = locale_msg[s.locale]
    # tolist = []
    # if s.emailattendant:
    #     tolist.append(s.emailattendant)
    # if s.emailparent:
    #     tolist.append(s.emailparent)
    # if s.emailplayer:
    #     tolist.append(s.emailplayer)
    # sub['champ'] = i18n['To be confirmed']
    # if s.nationality == 'BEL':
    #     sub['champ'] = i18n['Yes']
    # elif s.nationality :
    #     sub['champ'] = i18n['No']
    # tpath = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'templates')
    # with open(os.path.join(tpath,f'mailenrollment_{s.locale}.md')) as tmpl:
    #     plaintext = tmpl.read()
    # plaintext = plaintext.format(**sub)

    # # fetch the subject from the first line
    # subject = plaintext.split('\n')[0][3:]
    # htmltext = md.convert(plaintext)

    # # create message and send it
    # try:
    #     msg = MIMEMultipart('related')
    #     msg['Subject'] = subject
    #     msg['From'] = settings.EMAIL['sender']
    #     msg['To'] = ','.join(tolist)
    #     if settings.EMAIL.get('bcc'):
    #         msg['Bcc'] = settings.EMAIL['bcc']
    #     msg.preamble = 'This is a multi-part message in MIME format.'
    #     msgAlternative = MIMEMultipart('alternative')
    #     msgText = MIMEText(plaintext)
    #     msgAlternative.attach(msgText)
    #     msgHtml = MIMEText(htmltext, 'html')
    #     msgAlternative.attach(msgHtml)
    #     msg.attach(msgAlternative)
    #     backend = backends[settings.EMAIL['backend']]()
    #     send_message(msg)
    #     log.info(f'confirmation mail sent for {s.first_name} {s.last_name}')
    # except:
    #     log.exception(f'confirmation mail failed {s.first_name} {s.last_name}')
