#    Copyright 2019 - 2022 Chessdevil Consulting

import os, os.path
import yaml
from pathlib import Path

# paths
SHARED_PATH = Path(os.environ.get("SHARED_PATH", "../bycco_shared"))
SECRETS_PATH = Path(os.environ.get("SECRETS_PATH", ""))
TEMPLATES_PATH = Path(os.environ.get("TEMPLATES_PATH", "./bycco/templates"))


APIKEY = os.environ.get("APIKEY", "Jimi")
COLORLOG = False
DEBUG = os.environ.get("DEBUG_BYCCO", False)

EMAIL = {
    "backend": os.environ.get("EMAIL_BACKEND", "GMAIL"),
    "sender": "ruben.decrop@bycco.be",
    "bcc_reservation": "ruben.decrop@gmail.com,floreal@bycco.be",
    "bcc_enrollment": "ruben.decrop@gmail.com,luc.cornet@bycco.be",
    "gmail_file": os.environ.get("GMAIL_FILE", "credentials-gmail.json"),
    "account": "ruben.decrop@bycco.be",
}

LOG_CONFIG = {
    "version": 1,
    "formatters": {
        "simple": {
            "format": "%(levelname)s: %(asctime)s %(name)s %(message)s",
        },
        "color": {
            "format": "%(log_color)s%(levelname)s%(reset)s: %(asctime)s %(bold)s%(name)s%(reset)s %(message)s",
            "()": "reddevil.core.colorlogfactory.c_factory",
        },
    },
    "handlers": {
        "console": {
            "class": "logging.StreamHandler",
            "level": "INFO",
            "formatter": "simple",
            "stream": "ext://sys.stderr",
        }
    },
    "loggers": {
        "bycco": {
            "handlers": ["console"],
            "level": "INFO",
            "propagate": False,
        },
        "reddevil": {
            "handlers": ["console"],
            "level": "INFO",
            "propagate": False,
        },
        "fastapi": {
            "handlers": ["console"],
            "level": "INFO",
            "propagate": False,
        },
        "uvicorn": {
            "handlers": ["console"],
            "level": "INFO",
            "propagate": False,
        },
    },
}


GOOGLEDRIVE_I18N_ID = "1Rnklu5u0X-FSNr2rgsaFXy-AfHjG4F1cjI76QRrFx74"

# need to move to secrets
GOOGLE_CLIENT_ID = (
    "464711449307-7j2oecn3mkfs1eh3o7b5gh8np3ebhrdp.apps.googleusercontent.com"
)
GOOGLE_LOGIN_DOMAINS = ["bycco.be"]
GOOGLE_PROJECT_ID = os.environ.get("GOOGLE_PROJECT", "byccowebsiteprod")


SECRETS = {
    "mongodb": {
        "name": "bycco-mongodb",
        "manager": "googlejson",
    },
    "mysql": {
        "name": "bycco-mysql",
        "manager": "googlejson",
    },
    "gmail": {
        "name": "bycco-gmail",
        "manager": "googlejson",
    },
}

TOKEN = {
    "timeout": 180,  # timeout in minutes
    "secret": "Ramskappelle",
    "algorithm": "HS256",
    "nocheck": True,
}

JWT_ALGORITHM = "HS256"
JWT_SECRET = "tiswittvoaltuutdeluchtentendigtopoazen"

try:
    from local_settings import *

    ls = "local settings loaded"
except ImportError:
    ls = "No local settings found"

with open(SHARED_PATH / "content" / "common.yaml") as file:
    COMMON = yaml.load(file, Loader=yaml.FullLoader)

with open(SHARED_PATH / "data" / "prizes.yaml") as file:
    PRIZES = yaml.load(file, Loader=yaml.FullLoader)


if COLORLOG:
    LOG_CONFIG["handlers"]["console"]["formatter"] = "color"  # type: ignore
if DEBUG:
    LOG_CONFIG["handlers"]["console"]["level"] = "DEBUG"  # type: ignore
    LOG_CONFIG["loggers"]["bycco"]["level"] = "DEBUG"  # type: ignore
    LOG_CONFIG["loggers"]["reddevil"]["level"] = "DEBUG"  # type: ignore
