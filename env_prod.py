import os

COLORLOG = True

COMMON_PATH = "../content/common.yaml"

EMAIL = {
    "backend": os.environ.get("EMAIL_BACKEND", "GMAIL"),
    "sender": "ruben.decrop@bycco.be",
    "bcc_reservation": "ruben.decrop@gmail.com,floreal@bycco.be",
    "bcc_enrollment": "ruben.decrop@gmail.com,luc.cornet@gmail.com",
    "gmail_file": os.environ.get("GMAIL_FILE", "credentials-gmail.json"),
    "account": "ruben.decrop@bycco.be",
}
SECRETS = {
    "mongodb": {
        "name": "bycco-mongodb-prod",
        "manager": "filejson",
    },
    "mysql": {
        "name": "bycco-mysql",
        "manager": "filejson",
    },
    "gmail": {
        "name": "bycco-gmail-prod",
        "manager": "filejson",
    },
}

MODE = "production"