COLORLOG = True

COMMON_PATH = "../content/common.yaml"
EMAIL = {
    "backend": "SMTP",
    "sender": "noreply@bycco.be",
    "account": "ruben.decrop@bycco.be",
    "port": "1025",
    "host": "maildev.decrop.net",
    "bcc_reservation": "ruben.decrop@gmail.com,floreal@bycco.be",
    "bcc_enrollment": "ruben.decrop@gmail.com,luc.cornet@gmail.com",
}

SECRETS = {
    "mongodb": {
        "name": "bycco-mongodb-dev",
        "manager": "filejson",
    },
    "mysql": {
        "name": "bycco-mysql",
        "manager": "filejson",
    },
    "gmail": {
        "name": "bycco-gmail-test",
        "manager": "filejson",
    },
}


MODE = "local"