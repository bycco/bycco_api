# Shared Folder

This directory contains the the files that are shared between bycco_api and bycco_ui

It should be left empty

The deployment script will:

- copy the bycco_shared folder to here
- run the app deploy
- empty the this folder (so nothing is made persistent).
